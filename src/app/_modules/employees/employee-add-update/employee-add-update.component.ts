import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { validateAllFormFields } from 'src/app/_helpers';
import { Company, Role, Zone } from 'src/app/_models';
import { CompanyService, EmployeeService, RoleService, ZoneService } from 'src/app/_services';

@Component({
  selector: 'app-employee-add-update',
  templateUrl: './employee-add-update.component.html',
  styleUrls: ['./employee-add-update.component.scss']
})
export class EmployeeAddUpdateComponent implements OnInit {

  lookupZoneObserver: Observable<any[]> | null | undefined;
  lookupCompanyObserver: Observable<any[]> | null | undefined;
  lookupRoleObserver: Observable<any[]> | null | undefined;
  lookupZones: Zone[] = [];
  lookupCompanies: Company[] = [];
  lookupRoles: Role[] = [];
  
  loading: boolean = false;
  updated: boolean = false;
  errors: any;

  // Initializing Form with fields
  employeeForm = new FormGroup({
    id: new FormControl(null, []),
    username: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required]),
    name: new FormControl('', [Validators.required]),
    cnic: new FormControl('', [Validators.required]),
    designation: new FormControl('', [Validators.required]),
    phone_number: new FormControl('', [Validators.required]),
    address: new FormControl('', [Validators.required]),
    salary: new FormControl('', [Validators.required]),
    zone_id: new FormControl('', [Validators.required]),
    company_id: new FormControl('', [Validators.required]),
    // department_id: new FormControl('', [Validators.required]),
    role_type: new FormControl('', [Validators.required]),
    status: new FormControl('', [Validators.required]),
  });

  constructor(
    public dialogRef: MatDialogRef<EmployeeAddUpdateComponent>,
    private employeeService: EmployeeService,
    private zoneService: ZoneService,
    private companyService: CompanyService,
    private roleService: RoleService,
    @Inject(MAT_DIALOG_DATA) data: any) {
    if (data) this.employeeForm.get("id")?.setValue(data.id)
  }

  ngOnInit(): void {
    this.loadZones();
    this.loadCompanies();
    this.loadRoles();
    if (this.employeeForm.get("id")?.value) {
      this.loading = true;
      this.employeeService.find(this.employeeForm.get("id")?.value).subscribe(
        data => {
          this.fillForm(data)
          this.loading = false
        }
      )
    }
  }

  loadZones() {
    this.lookupZoneObserver = this.zoneService.findAll(0, [], '', '', -1);
    this.lookupZoneObserver.subscribe(
      data => {
        console.log(data)
        this.lookupZones = data;
      },
      err => {
        console.log(err)
      }
    )
  }

  loadCompanies() {
    this.lookupCompanyObserver = this.companyService.findAll(0, [], '', '', -1);
    this.lookupCompanyObserver.subscribe(
      data => {
        console.log(data)
        this.lookupCompanies = data;
      },
      err => {
        console.log(err)
      }
    )
  }

  loadRoles() {
    this.lookupRoleObserver = this.roleService.findAll(0, [], '', '', -1);
    this.lookupRoleObserver.subscribe(
      data => {
        console.log(data)
        this.lookupRoles = data;
      },
      err => {
        console.log(err)
      }
    )
  }

  fillForm(product: any) {
    Object.keys(this.employeeForm.controls).forEach((key, index) => {
      this.employeeForm.controls[key].markAsDirty();
      this.employeeForm.controls[key].patchValue(product[key], {
        onlySelf: true,
      });
    });
  }

  onSubmit(): void {
    if (this.employeeForm.valid) {
      if (!this.employeeForm.get("id")?.value) {
        this.employeeService.store(this.employeeForm.value).subscribe(
          data => {
            console.log(data)
            this.onClose()
          },
          err => {
            console.log(err)
            this.errors = err.errors
          }
        )
      } else {
        this.employeeService.update(this.employeeForm.value).subscribe(
          data => {
            console.log(data)
            this.updated = true
            this.onClose()
          },
          err => {
            console.log(err)
            this.errors = err.errors
          }
        )
      }
    } else
      validateAllFormFields(this.employeeForm);
  }

  onClear(): void {
    this.employeeForm.reset()
  }

  onClose() {
    this.dialogRef.close({ updated: this.updated })
  }

  // Getters for form fields
  get username() {
    return this.employeeForm.get('username')
  }
  get password() {
    return this.employeeForm.get('password')
  }
  get name() {
    return this.employeeForm.get('name')
  }
  get cnic() {
    return this.employeeForm.get('cnic')
  }
  get designation() {
    return this.employeeForm.get('designation')
  }
  get phone_number() {
    return this.employeeForm.get('phone_number')
  }
  get address() {
    return this.employeeForm.get('address')
  }
  get salary() {
    return this.employeeForm.get('salary')
  }
  get zone_id() {
    return this.employeeForm.get('zone_id')
  }
  get company_id() {
    return this.employeeForm.get('company_id')
  }
  // get department_id() {
  //   return this.employeeForm.get('department_id')
  // }
  get role_type() {
    return this.employeeForm.get('role_type')
  }
  get status() {
    return this.employeeForm.get('status')
  }
}
