import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { merge } from 'rxjs';
import { tap } from 'rxjs/operators';
import { EmployeeService } from 'src/app/_services';
import { EmployeeAddUpdateComponent } from '../employee-add-update/employee-add-update.component';
import { EmployeeDatasource } from './employee-datasource';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.scss']
})
export class EmployeeListComponent implements OnInit {

  dataSource: EmployeeDatasource | any;

  displayedColumns = ["id", "employee_id", "employeeName", "actions"];

  @ViewChild(MatPaginator) paginator: MatPaginator | any;
  @ViewChild(MatSort)
  sort: MatSort = new MatSort;

  @ViewChild('reload') reload: ElementRef | undefined;

  constructor(
    private employeeService: EmployeeService,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.dataSource = new EmployeeDatasource(this.employeeService);
  }

  ngAfterViewInit() {
    Promise.resolve().then(() => {

      this.dataSource?.loadEmployee(0, [], '', 'asc', 1, 10);

      this.sort?.sortChange.subscribe(() => this.paginator.pageIndex = 0);

      merge(this.sort.sortChange, this.paginator.page)
        .pipe(
          tap(() => this.loadEmployeePage())
        )
        .subscribe();
    });
  }

  loadEmployeePage() {
    console.log('loadEmployeePage()');
    console.log(this.paginator.pageIndex)
    this.dataSource?.loadEmployee(
      0,
      [
        // { "col": "crc", "op" : "like", "val": this.filterCrc?.nativeElement.value},
        // { "col": "name", "op" : "like", "val": this.filterName?.nativeElement.value}
      ],
      this.sort.active,
      this.sort.direction,
      this.paginator.pageIndex + 1,
      this.paginator.pageSize);
  }

  onEmployee() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.width = "930px";
    dialogConfig.autoFocus = false;
    dialogConfig.data = {};
    const dialogRef = this.dialog.open(EmployeeAddUpdateComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(
      data => {
        this.loadEmployeePage()
      }
    );
  }

  onEdit(row: any) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.width = "930px";
    dialogConfig.autoFocus = false;
    dialogConfig.data = {
      id: row.id
    };
    const dialogRef = this.dialog.open(EmployeeAddUpdateComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(
      data => {
        this.loadEmployeePage()
      }
    );
  }
}
