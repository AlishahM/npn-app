import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EmployeeAddUpdateComponent } from './employee-add-update/employee-add-update.component';
import { EmployeeListComponent } from './employee-list/employee-list.component';
import { EmployeesComponent } from './employees.component';

const routes: Routes = [{
  path: '', component: EmployeesComponent, children: [
    { path: 'employees-list', component: EmployeeListComponent },
    { path: 'add-employee', component: EmployeeAddUpdateComponent },
    { path: '', redirectTo: 'employees-list' }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmployeesRoutingModule { }
