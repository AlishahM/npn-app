import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GiftingExpenditureReportComponent } from './gifting-expenditure-report/gifting-expenditure-report.component';
import { PromotionalMaterialReportComponent } from './promotional-material-report/promotional-material-report.component';
import { ReportsComponent } from './reports.component';

const routes: Routes = [{ path: '', component: ReportsComponent, children: [
  { path: 'promotional-material-report', component: PromotionalMaterialReportComponent },
  { path: '', redirectTo: 'promotional-material-report' },
  { path: 'gifting-expenditure-report', component: GiftingExpenditureReportComponent },
  { path: '', redirectTo: 'gifting-expenditure-report' }
] }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportsRoutingModule { }
