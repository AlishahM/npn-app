import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GiftingExpenditureReportComponent } from './gifting-expenditure-report.component';

describe('GiftingExpenditureReportComponent', () => {
  let component: GiftingExpenditureReportComponent;
  let fixture: ComponentFixture<GiftingExpenditureReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GiftingExpenditureReportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GiftingExpenditureReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
