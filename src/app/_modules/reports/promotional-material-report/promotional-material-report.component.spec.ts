import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PromotionalMaterialReportComponent } from './promotional-material-report.component';

describe('PromotionalMaterialReportComponent', () => {
  let component: PromotionalMaterialReportComponent;
  let fixture: ComponentFixture<PromotionalMaterialReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PromotionalMaterialReportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PromotionalMaterialReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
