import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReportsRoutingModule } from './reports-routing.module';
import { ReportsComponent } from './reports.component';
import { PromotionalMaterialReportComponent } from './promotional-material-report/promotional-material-report.component';
import { GiftingExpenditureReportComponent } from './gifting-expenditure-report/gifting-expenditure-report.component';


@NgModule({
  declarations: [
    ReportsComponent,
    PromotionalMaterialReportComponent,
    GiftingExpenditureReportComponent
  ],
  imports: [
    CommonModule,
    ReportsRoutingModule
  ]
})
export class ReportsModule { }
