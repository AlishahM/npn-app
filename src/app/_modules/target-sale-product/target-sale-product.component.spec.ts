import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TargetSaleProductComponent } from './target-sale-product.component';

describe('TargetSaleProductComponent', () => {
  let component: TargetSaleProductComponent;
  let fixture: ComponentFixture<TargetSaleProductComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TargetSaleProductComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TargetSaleProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
