import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TargetSaleProductRoutingModule } from './target-sale-product-routing.module';
import { TargetSaleProductComponent } from './target-sale-product.component';
import { TargetSaleProductListComponent } from './target-sale-product-list/target-sale-product-list.component';


@NgModule({
  declarations: [
    TargetSaleProductComponent,
    TargetSaleProductListComponent
  ],
  imports: [
    CommonModule,
    TargetSaleProductRoutingModule
  ]
})
export class TargetSaleProductModule { }
