import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TargetSaleProductListComponent } from './target-sale-product-list.component';

describe('TargetSaleProductListComponent', () => {
  let component: TargetSaleProductListComponent;
  let fixture: ComponentFixture<TargetSaleProductListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TargetSaleProductListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TargetSaleProductListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
