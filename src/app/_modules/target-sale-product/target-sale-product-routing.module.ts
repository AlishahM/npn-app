import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TargetSaleProductListComponent } from './target-sale-product-list/target-sale-product-list.component';
import { TargetSaleProductComponent } from './target-sale-product.component';

const routes: Routes = [{ path: '', component: TargetSaleProductComponent, children: [
  { path: 'target-sale-product-list', component: TargetSaleProductListComponent },
  { path: '', redirectTo: 'target-sale-product-list' }
] }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TargetSaleProductRoutingModule { }
