import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GrowthReportListComponent } from './growth-report-list/growth-report-list.component';
import { GrowthReportComponent } from './growth-report.component';

const routes: Routes = [{ path: '', component: GrowthReportComponent, children: [
  { path: 'growth-report-list', component: GrowthReportListComponent },
  { path: '', redirectTo: 'growth-report-list' }
] }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GrowthReportRoutingModule { }
