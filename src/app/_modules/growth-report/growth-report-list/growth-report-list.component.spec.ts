import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GrowthReportListComponent } from './growth-report-list.component';

describe('GrowthReportListComponent', () => {
  let component: GrowthReportListComponent;
  let fixture: ComponentFixture<GrowthReportListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GrowthReportListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GrowthReportListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
