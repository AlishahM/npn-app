import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GrowthReportRoutingModule } from './growth-report-routing.module';
import { GrowthReportComponent } from './growth-report.component';
import { GrowthReportListComponent } from './growth-report-list/growth-report-list.component';


@NgModule({
  declarations: [
    GrowthReportComponent,
    GrowthReportListComponent
  ],
  imports: [
    CommonModule,
    GrowthReportRoutingModule
  ]
})
export class GrowthReportModule { }
