import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { validateAllFormFields } from 'src/app/_helpers';
import { UserTypes } from 'src/app/_models';
import { AuthenticationService } from 'src/app/_services';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loading: boolean = false;
  errors: any;

  constructor(
    private authenticationService: AuthenticationService,
    private router: Router
  ) {
    let employee: any = this.authenticationService.currentEmployeeValue;
    if (Object.entries(employee).length != 0) {
      this.router.navigate(['/dashboard']);
    } else {
      this.router.navigate(['/login']);
    }
  }

  // Initializing Form with fields
  loginForm = new FormGroup({
    username: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required]),
  });

  // Getters for form fields
  get username() {
    return this.loginForm.get('username');
  }
  get password() {
    return this.loginForm.get('password');
  }

  ngOnInit(): void {
  }

  onSubmit(): void {
    if (this.loginForm.valid) {
      this.loading = true;
      this.authenticationService.login(this.loginForm.value)
        .subscribe(
          (employee: any) => {
            this.loading = false;
            switch (employee.user.role_type) {
              case UserTypes.ADMIN:
              case UserTypes.EMPLOYEE:
              case UserTypes.DISTRIBUTOR:
              case UserTypes.MANAGER:
                this.router.navigate(['/dashboard']);
                break;
              default:
                this.router.navigate(['/login']);
                break;
            }
          },
          (error: any) => {
            this.loading = false;
            // this.errors = error.error.errors;
          }
        );
    } else
      validateAllFormFields(this.loginForm);
  }

}
