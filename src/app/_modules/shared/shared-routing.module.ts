import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';
import { SharedComponent } from './shared.component';

const routes: Routes = [
  { path: '', redirectTo: 'login' },
  {
    path: '', component: SharedComponent,
    children: [
      { path: 'dashboard', component: DashboardComponent },
      { path: 'products', loadChildren: () => import('../products/products.module').then(m => m.ProductsModule) },
      { path: 'company', loadChildren: () => import('../company/company.module').then(m => m.CompanyModule) },
      { path: 'zone', loadChildren: () => import('../zone/zone.module').then(m => m.ZoneModule) },
      { path: 'employees', loadChildren: () => import('../employees/employees.module').then(m => m.EmployeesModule) },
      { path: 'memorandum', loadChildren: () => import('../memorandum/memorandum.module').then(m => m.MemorandumModule) },
      { path: 'sales-expense', loadChildren: () => import('../sales-expense/sales-expense.module').then(m => m.SalesExpenseModule) },
      { path: 'promotional-material', loadChildren: () => import('../promotional-material/promotional-material.module').then(m => m.PromotionalMaterialModule) },
      { path: 'monthly-work-plan', loadChildren: () => import('../monthly-work-plan/monthly-work-plan.module').then(m => m.MonthlyWorkPlanModule) },
      { path: 'distributor-ledger', loadChildren: () => import('../distributor-ledger/distributor-ledger.module').then(m => m.DistributorLedgerModule) },
      { path: 'reports', loadChildren: () => import('../reports/reports.module').then(m => m.ReportsModule) },
      { path: 'product-analysis', loadChildren: () => import('../product-analysis/product-analysis.module').then(m => m.ProductAnalysisModule) },
      { path: 'daily-call-report', loadChildren: () => import('../daily-call-report/daily-call-report.module').then(m => m.DailyCallReportModule) },
      { path: 'purchase-order', loadChildren: () => import('../purchase-order/purchase-order.module').then(m => m.PurchaseOrderModule) },
      { path: 'doctors', loadChildren: () => import('../doctors/doctors.module').then(m => m.DoctorsModule) },
      { path: 'product-analysis', loadChildren: () => import('../product-analysis/product-analysis.module').then(m => m.ProductAnalysisModule) },
      { path: 'purchase-order', loadChildren: () => import('..//purchase-order/purchase-order.module').then(m => m.PurchaseOrderModule) },
      { path: 'growth-report', loadChildren: () => import('../growth-report/growth-report.module').then(m => m.GrowthReportModule) },
      { path: 'stock-analysis', loadChildren: () => import('../stock-analysis/stock-analysis.module').then(m => m.StockAnalysisModule) },
      { path: 'budget-distribution', loadChildren: () => import('../budget-distribution/budget-distribution.module').then(m => m.BudgetDistributionModule) },
      { path: 'target-assignment', loadChildren: () => import('../target-assignment/target-assignment.module').then(m => m.TargetAssignmentModule) },
      { path: 'target-sale-product', loadChildren: () => import('../target-sale-product/target-sale-product.module').then(m => m.TargetSaleProductModule) },
      { path: 'sale-graph', loadChildren: () => import('../sale-graph/sale-graph.module').then(m => m.SaleGraphModule) },
      { path: 'attendance', loadChildren: () => import('../attendance/attendance.module').then(m => m.AttendanceModule) },
      { path: 'client-investment-ledger', loadChildren: () => import('../client-investment-ledger/client-investment-ledger.module').then(m => m.ClientInvestmentLedgerModule) },
    ]
  },
  { path: 'login', component: LoginComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SharedRoutingModule { }
