import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/_services';

@Component({
  selector: 'app-shared',
  templateUrl: './shared.component.html',
  styleUrls: ['./shared.component.scss']
})
export class SharedComponent implements OnInit {

  employee: any

  constructor(
    private authenticationService: AuthenticationService, private router: Router
  ) {
    this.employee = this.authenticationService.currentEmployeeValue;
    if (Object.entries(this.employee).length === 0)
      this.router.navigate(['login']);
  }

  ngOnInit(): void {
  }

}
