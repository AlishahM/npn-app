import { Component, OnInit } from '@angular/core';
import { UserTypes } from 'src/app/_models';
import { Employees } from 'src/app/_models/employees';
import { AuthenticationService } from 'src/app/_services';

@Component({
  selector: 'app-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.scss']
})
export class SideMenuComponent implements OnInit {

  employee: any;
  public UserTypes = UserTypes;

  constructor(
    private authenticationService: AuthenticationService
  ) {
    this.employee = this.authenticationService.currentEmployeeValue;
  }

  ngOnInit(): void {
  }

}
