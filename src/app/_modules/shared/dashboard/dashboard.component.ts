import { Component, OnInit } from '@angular/core';
import { UserTypes } from 'src/app/_models';
import { AuthenticationService } from 'src/app/_services';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  employee: any;
  public UserTypes = UserTypes;

  constructor(
    private authenticationService: AuthenticationService
  ) {
    this.employee = this.authenticationService.currentEmployeeValue;
  }

  ngOnInit(): void {
  }

}
