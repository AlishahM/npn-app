import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/_services';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {

  employee: any

  constructor(
    private authenticationService: AuthenticationService
  ) {
    this.employee = this.authenticationService.currentEmployeeValue;
  }

  ngOnInit(): void {
  }

  logout() {
    this.authenticationService.logout();
  }
}
