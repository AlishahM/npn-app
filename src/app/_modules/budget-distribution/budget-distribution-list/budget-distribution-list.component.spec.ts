import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BudgetDistributionListComponent } from './budget-distribution-list.component';

describe('BudgetDistributionListComponent', () => {
  let component: BudgetDistributionListComponent;
  let fixture: ComponentFixture<BudgetDistributionListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BudgetDistributionListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BudgetDistributionListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
