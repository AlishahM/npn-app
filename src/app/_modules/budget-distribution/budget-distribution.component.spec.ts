import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BudgetDistributionComponent } from './budget-distribution.component';

describe('BudgetDistributionComponent', () => {
  let component: BudgetDistributionComponent;
  let fixture: ComponentFixture<BudgetDistributionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BudgetDistributionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BudgetDistributionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
