import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BudgetAddUpdateComponent } from './budget-add-update/budget-add-update.component';
import { BudgetDistributionListComponent } from './budget-distribution-list/budget-distribution-list.component';
import { BudgetDistributionComponent } from './budget-distribution.component';

const routes: Routes = [{ path: '', component: BudgetDistributionComponent, children: [
  { path: 'budget-distribution-list', component: BudgetDistributionListComponent },
  { path: 'budget-add-update', component: BudgetAddUpdateComponent },
  { path: '', redirectTo: 'budget-distribution-list' }
] }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BudgetDistributionRoutingModule { }
