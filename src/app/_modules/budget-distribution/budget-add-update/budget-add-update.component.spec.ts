import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BudgetAddUpdateComponent } from './budget-add-update.component';

describe('BudgetAddUpdateComponent', () => {
  let component: BudgetAddUpdateComponent;
  let fixture: ComponentFixture<BudgetAddUpdateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BudgetAddUpdateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BudgetAddUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
