import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BudgetDistributionRoutingModule } from './budget-distribution-routing.module';
import { BudgetDistributionComponent } from './budget-distribution.component';
import { BudgetDistributionListComponent } from './budget-distribution-list/budget-distribution-list.component';
import { BudgetAddUpdateComponent } from './budget-add-update/budget-add-update.component';


@NgModule({
  declarations: [
    BudgetDistributionComponent,
    BudgetDistributionListComponent,
    BudgetAddUpdateComponent
  ],
  imports: [
    CommonModule,
    BudgetDistributionRoutingModule
  ]
})
export class BudgetDistributionModule { }
