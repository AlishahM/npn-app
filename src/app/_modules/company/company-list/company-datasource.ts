import {CollectionViewer, DataSource} from "@angular/cdk/collections";
import {Observable, BehaviorSubject, of} from "rxjs";
import {catchError, finalize} from "rxjs/operators";
import { Company } from "src/app/_models";

import { CompanyService } from '../../../_services';



export class CompanyDatasource implements DataSource<Company> {

    totalItems = 0;
    
    private CompanysSubject = new BehaviorSubject<Company[]>([]);

    private loadingSubject = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingSubject.asObservable();

    constructor(private companyService: CompanyService) {

    }

    loadCompanys(courseId:number,
                filters:any[],
                sortBy:string,
                sortDirection:string,
                pageNumber:number,
                pageSize:number) {

        this.loadingSubject.next(true);
        this.companyService.findAll(courseId, filters, sortBy, sortDirection,
            pageNumber, pageSize).pipe(
                catchError(() => of([])),
                finalize(() => this.loadingSubject.next(false))
            )
            .subscribe(data => {
                this.totalItems = data.total;
                return this.CompanysSubject.next(data.data);
			});

    }

    connect(collectionViewer: CollectionViewer): Observable<Company[]> {
		// bind observer
        console.log("Connecting data source");
        return this.CompanysSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.CompanysSubject.complete();
        this.loadingSubject.complete();
    }

}

