import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { CompanyAddUpdateComponent } from '../company-add-update';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { CompanyDatasource } from './company-datasource';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ActivatedRoute } from '@angular/router';
import { CompanyService } from 'src/app/_services';
import { merge } from 'rxjs';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-company-list',
  templateUrl: './company-list.component.html',
  styleUrls: ['./company-list.component.scss']
})
export class CompanyListComponent implements OnInit {

// loading = false;

dataSource: CompanyDatasource | any;

displayedColumns = ["id", "name", "zone_id", "actions"];

@ViewChild(MatPaginator) paginator: MatPaginator | any;
@ViewChild(MatSort)
sort: MatSort = new MatSort;

// @ViewChild('filterId') filterId: ElementRef | undefined;
// @ViewChild('filterName') filterName: ElementRef | undefined;
// @ViewChild('filterCrc') filterCrc: ElementRef | undefined;
// @ViewChild('filterDescription') filterDescription: ElementRef | undefined;
// @ViewChild('filterOthers') filterOthers: ElementRef | undefined;
@ViewChild('reload') reload: ElementRef | undefined;

constructor(
    private route: ActivatedRoute,
    private companyService: CompanyService,
    private dialog: MatDialog,
    //private snackBService:SnackBarService
) {
}

ngOnInit() {
    this.dataSource = new CompanyDatasource(this.companyService);
}

ngAfterViewInit() {
    Promise.resolve().then(() => {

        this.dataSource?.loadCompanys(0, [], '', 'asc', 1, 10);

        this.sort?.sortChange.subscribe(() => this.paginator.pageIndex = 0);



        // fromEvent(this.filterName?.nativeElement,'change')
        //     .pipe(
        //         debounceTime(150), 
        //         distinctUntilChanged(),
        //         tap(() => {
        //             this.paginator.pageIndex = 0;
        //             this.loadCompanysPage();
        //         })
        //     )
        //     .subscribe();

        // fromEvent(this.filterCrc?.nativeElement,'change')
        //     .pipe(
        //         debounceTime(150), 
        //         distinctUntilChanged(),
        //         tap(() => {
        //             this.paginator.pageIndex = 0;
        //             this.loadCompanysPage();
        //         })
        //     )
        //     .subscribe(); 


        // fromEvent(this.filterDescription?.nativeElement,'change')
        //     .pipe(
        //         debounceTime(150), 
        //         distinctUntilChanged(),
        //         tap(() => {
        //             this.paginator.pageIndex = 0;
        //             this.loadCompanysPage();
        //         })
        //     )
        //     .subscribe();


        // fromEvent(this.filterOthers?.nativeElement,'change')
        //     .pipe(
        //         debounceTime(150), 
        //         distinctUntilChanged(),
        //         tap(() => {
        //             this.paginator.pageIndex = 0;
        //             this.loadCompanysPage();
        //         })
        //     )
        //     .subscribe();

        merge(this.sort.sortChange, this.paginator.page)
            .pipe(
                tap(() => this.loadCompanysPage())
            )
            .subscribe();
    });
}

loadCompanysPage() {
    console.log('loadCompanysPage()');
    console.log(this.paginator.pageIndex)
    this.dataSource?.loadCompanys(
        0,
        [
            // { "col": "crc", "op" : "like", "val": this.filterCrc?.nativeElement.value},
            // { "col": "name", "op" : "like", "val": this.filterName?.nativeElement.value}
        ],
        this.sort.active,
        this.sort.direction,
        this.paginator.pageIndex + 1,
        this.paginator.pageSize);
}

onZones(zone: any) {
  let zones: string = ""
  zone.forEach((element: any, index: number) => {
    if(index!=0) zones+=", "
    zones+=element.zone.zone
  });
  return zones;
}

onCompany() {
  const dialogConfig = new MatDialogConfig();
  dialogConfig.width = "930px";
  dialogConfig.autoFocus = false;
  dialogConfig.data = {};
  const dialogRef = this.dialog.open(CompanyAddUpdateComponent, dialogConfig);
  dialogRef.afterClosed().subscribe(
      data => {
        this.loadCompanysPage()
      }
  );
}

onEdit(row: any) {
  console.log(row)
  const dialogConfig = new MatDialogConfig();
  dialogConfig.width = "930px";
  dialogConfig.autoFocus = false;
  dialogConfig.data = {
    id: row.id
  };
  const dialogRef = this.dialog.open(CompanyAddUpdateComponent, dialogConfig);
  dialogRef.afterClosed().subscribe(
      data => {
        if(data) this.loadCompanysPage()
      }
  );
}

}
