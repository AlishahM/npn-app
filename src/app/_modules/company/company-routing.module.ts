import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CompanyAddUpdateComponent } from './company-add-update/company-add-update.component';
import { CompanyListComponent } from './company-list/company-list.component';
import { CompanyComponent } from './company.component';

const routes: Routes = [
  {
    path: '', component: CompanyComponent, children: [
      { path: 'company-list', component: CompanyListComponent },
      { path: 'add-company', component: CompanyAddUpdateComponent },
      { path: '', redirectTo: 'company-list' }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CompanyRoutingModule { }
