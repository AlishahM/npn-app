import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { Company, Zone } from 'src/app/_models';
import { CompanyService, ZoneService } from 'src/app/_services';
import { CompanyComponent } from '../company.component';

@Component({
  selector: 'app-company-add-update',
  templateUrl: './company-add-update.component.html',
  styleUrls: ['./company-add-update.component.scss']
})
export class CompanyAddUpdateComponent implements OnInit {

  lookupZoneObserver: Observable<any[]> | null | undefined;
  lookupZones: Zone[] = [];

  id: any;
  loading: boolean = false;
  updated: boolean = false;
  errors: any;

  zoneList: string[] = ['Z-101', 'Z-102', 'Z-103', 'Z-104', 'Z-105', 'Z-106'];

  // Initializing Form with fields
  companyForm = new FormGroup({
    id: new FormControl(null, []),
    name: new FormControl('', [Validators.required]),
    zone_id: new FormControl('', [Validators.required]),
  });

  constructor(
    public dialogRef: MatDialogRef<CompanyComponent>,
    private companyService: CompanyService,
    private zoneService: ZoneService,
    @Inject(MAT_DIALOG_DATA) data: any
  ) {
    if (data) {
      this.id = data.id
      this.companyForm.get("id")?.setValue(data.id)
    }
  }

  ngOnInit(): void {
    this.loading = true;
    this.loadZones();

    if (this.companyForm.get("id")?.value) {
      this.loading = true;
      this.companyService.find(this.companyForm.get("id")?.value).subscribe(
        data => {
          this.fillForm(data)
          this.loading = false
        }
      )
    }
    this.loading = false;
    return;
  }

  fillForm(product: any) {
    Object.keys(this.companyForm.controls).forEach((key, index) => {
      this.companyForm.controls[key].markAsDirty();
      this.companyForm.controls[key].patchValue(product[key], {
        onlySelf: true,
      });
    });
  }

  loadZones() {
    this.lookupZoneObserver = this.zoneService.findAll(0, [], '', '', -1);
    this.lookupZoneObserver.subscribe(
      data => {
        console.log(data)
        this.lookupZones = data;
      },
      err => {
        console.log(err)
      }
    )
  }

  onSubmit() {
    if (this.companyForm.valid) {
      if (!this.companyForm.get("id")?.value) {
        this.companyService.store(new Company().deserialize(this.companyForm.value).compose()).subscribe(
          data => {
            this.onClose()
          },
          err => {
            console.log(err)
          }
        )
      } else {
        this.companyService.update(new Company().deserialize(this.companyForm.value).compose()).subscribe(
          data => {
            this.onClose()
          },
          err => {
            console.log(err)
          }
        )
      }
    }
  }

  onClear(): void {
    this.companyForm.reset()
  }

  onClose() {
    this.dialogRef.close()
  }

  // Getters for form fields
  get name() {
    return this.companyForm.get('name')
  }
  get zone_id() {
    return this.companyForm.get('zone_id')
  }

}
