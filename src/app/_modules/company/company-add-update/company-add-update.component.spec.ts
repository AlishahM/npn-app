import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyAddUpdateComponent } from './company-add-update.component';

describe('CompanyAddUpdateComponent', () => {
  let component: CompanyAddUpdateComponent;
  let fixture: ComponentFixture<CompanyAddUpdateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CompanyAddUpdateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyAddUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
