import { Component, Inject, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { validateAllFormFields } from 'src/app/_helpers';
import { AuthenticationService, DoctorService } from 'src/app/_services';
import { WorkPlanService } from 'src/app/_services/work-plan/work-plan.service';

@Component({
  selector: 'app-work-plan-add-update',
  templateUrl: './work-plan-add-update.component.html',
  styleUrls: ['./work-plan-add-update.component.scss']
})
export class WorkPlanAddUpdateComponent implements OnInit {

  loading: boolean = false;
  updated: boolean = false;
  workPlanId?: number;
  errors: any;

  lookupDoctorObserver: Observable<any[]> | null | undefined;
  lookupDoctors: any[] = [];

  isEditMode: boolean = false;

  monthsList = [
    {id: 1, name: "January"},
    {id: 2, name: "Feburary"},
    {id: 3, name: "March"},
    {id: 4, name: "April"},
    {id: 5, name: "May"},
    {id: 6, name: "June"},
    {id: 7, name: "July"},
    {id: 8, name: "August"},
    {id: 9, name: "September"},
    {id: 10, name: "October"},
    {id: 11, name: "November"},
    {id: 12, name: "December"}
  ]

  yearsList = [
    {value: 2021},
    {value: 2022},
    {value: 2023},
    {value: 2024}
  ]

  workType = [
    {id: 1, name: "Local"},
    {id: 2, name: "Outstation"},
  ]

  doctorList = [
    {id: 1, name: "Doctor 1"},
    {id: 2, name: "Doctor 2"},
  ]

  workPlanForm: FormGroup = this.formBuilder.group({
    id: new FormControl("", []),
    user_id: new FormControl("", []),
    role_type: new FormControl("", []),
    from_date: new FormControl("", []),
    to_date: new FormControl("", []),
    month: new FormControl("", []),
    year: new FormControl("", []),
    work_plan_details: this.formBuilder.array([]),
  });

  constructor(
    public dialogRef: MatDialogRef<WorkPlanAddUpdateComponent>,
    private workPlanService: WorkPlanService,
    private doctorService: DoctorService,
    public formBuilder: FormBuilder,
    private authenticationService: AuthenticationService,
    @Inject(MAT_DIALOG_DATA) data: any
  ) {
    if (data.workPlanId) {
      this.isEditMode = true;
      this.workPlanId = data.workPlanId;
    }
    this.authenticationService.currentEmployee.subscribe(
      (data: any) => {
        console.log(data.user)
        this.user_id?.setValue(data.user.id)
        this.role_type?.setValue(data.user.role_type)
      }
    )
  }

  ngOnInit(): void {
    this.loadDoctors();
    if(this.isEditMode)
      this.getWorkPlanById()
  }

  loadDoctors() {
    this.doctorService.findAll(0, [], '' ,'', -1).subscribe(
      data => {
        this.lookupDoctors = data
      },
      err => {
        console.log(err)
      }
    )
  }

  getWorkPlanById(): void {
    this.loading = true;
    this.workPlanService.find(this.workPlanId).subscribe(
      data => {
        this.from_date?.setValue(data.from_date)
        this.to_date?.setValue(data.to_date)
        this.month?.setValue(data.month)
        this.year?.setValue(data.year)
        data.work_plan_details.forEach((element: any) => {
          this.addWorkPlan(element)
        });

        this.loading = false;
      }
    )
  }

  addWorkPlan(workPlan?: any) {
    if (!workPlan) {
      workPlan = <any>{};
    }

    console.log('workplan: ', workPlan)

    const workPlanGroup = new FormGroup({
      date: new FormControl("", []),
      revised_date: new FormControl("", []),
      work_type: new FormControl("", []),
      doctor: new FormControl("", []),
      morning_area: new FormControl("", []),
      morning_time: new FormControl("", []),
      evening_area: new FormControl("", []),
      evening_time: new FormControl("", []),
    });
    workPlanGroup.patchValue(workPlan, { onlySelf: true });
    this.work_plan_details.push(workPlanGroup);
  }

  removeWorkPlan(i: number) {
    this.work_plan_details.removeAt(i);
  }

  onSubmit(): void {
    if (this.workPlanForm.valid) {
      this.workPlanService.store(this.workPlanForm.value).subscribe(
        data => {
          console.log(data)
          this.onClose()
        },
        err => {
          console.log(err)
          this.errors = err.errors
        }
      )
    } else
      validateAllFormFields(this.workPlanForm);
  }

  onClear(): void {
    this.workPlanForm.reset()
  }

  onClose() {
    this.dialogRef.close({ updated: this.updated })
  }

  get user_id() {
    return this.workPlanForm.get("user_id");
  }

  get role_type() {
    return this.workPlanForm.get("role_type");
  }

  get from_date() {
    return this.workPlanForm.get("from_date");
  }

  get to_date() {
    return this.workPlanForm.get("to_date");
  }

  get month() {
    return this.workPlanForm.get("month");
  }

  get year() {
    return this.workPlanForm.get("year");
  }

  get work_plan_details(): FormArray {
    return this.workPlanForm.controls["work_plan_details"] as FormArray;
  }
}
