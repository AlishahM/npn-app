import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkPlanAddUpdateComponent } from './work-plan-add-update.component';

describe('WorkPlanAddUpdateComponent', () => {
  let component: WorkPlanAddUpdateComponent;
  let fixture: ComponentFixture<WorkPlanAddUpdateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WorkPlanAddUpdateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkPlanAddUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
