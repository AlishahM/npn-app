import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MonthlyWorkPlanComponent } from './monthly-work-plan.component';

describe('MonthlyWorkPlanComponent', () => {
  let component: MonthlyWorkPlanComponent;
  let fixture: ComponentFixture<MonthlyWorkPlanComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MonthlyWorkPlanComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MonthlyWorkPlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
