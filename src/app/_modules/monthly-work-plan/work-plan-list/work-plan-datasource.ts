import { CollectionViewer, DataSource } from "@angular/cdk/collections";
import { Observable, BehaviorSubject, of } from "rxjs";
import { catchError, finalize } from "rxjs/operators";
import { WorkPlanService } from "src/app/_services/work-plan/work-plan.service";

export class WorkPlanDatasource implements DataSource<any> {

    totalItems = 0;

    private workPlanSubject = new BehaviorSubject<any[]>([]);

    private loadingSubject = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingSubject.asObservable();

    constructor(private workPlanService: WorkPlanService) { }

    loadWorkPlan(workPlanId: number,
        filters: any[],
        sortBy: string,
        sortDirection: string,
        pageNumber: number,
        pageSize: number) {

        this.loadingSubject.next(true);
        this.workPlanService.findAll(workPlanId, filters, sortBy, sortDirection,
            pageNumber, pageSize).pipe(
                catchError(() => of([])),
                finalize(() => this.loadingSubject.next(false))
            )
            .subscribe((data:any) => {
                console.log(data)
                this.totalItems = data.total;
                return this.workPlanSubject.next(data.data);
            });

    }

    connect(collectionViewer: CollectionViewer): Observable<any[]> {
        // bind observer
        console.log("Connecting data source");
        return this.workPlanSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.workPlanSubject.complete();
        this.loadingSubject.complete();
    }

}

