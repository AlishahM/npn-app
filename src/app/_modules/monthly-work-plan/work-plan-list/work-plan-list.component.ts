import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { merge } from 'rxjs';
import { tap } from 'rxjs/operators';
import { UserTypes } from 'src/app/_models';
import { AuthenticationService } from 'src/app/_services';
import { WorkPlanService } from 'src/app/_services/work-plan/work-plan.service';
import { WorkPlanAddUpdateComponent } from '../work-plan-add-update/work-plan-add-update.component';
import { WorkPlanDatasource } from './work-plan-datasource';

@Component({
  selector: 'app-work-plan-list',
  templateUrl: './work-plan-list.component.html',
  styleUrls: ['./work-plan-list.component.scss']
})
export class WorkPlanListComponent implements OnInit {

  dataSource: WorkPlanDatasource | any;
  user_id?: number;
  role_type?: number;

  displayedColumns = ["plan_no", "user.name", "from_date", "to_date", "month", "year", "actions"];

  @ViewChild(MatPaginator) paginator: MatPaginator | any;
  @ViewChild(MatSort)
  sort: MatSort = new MatSort;

  constructor(
    private dialog: MatDialog,
    private workPlanService: WorkPlanService,
    private authenticationService: AuthenticationService,
  ) { }

  ngOnInit(): void {
    this.dataSource = new WorkPlanDatasource(this.workPlanService);
    this.authenticationService.currentEmployee.subscribe(
      (data: any) => {
        console.log(data.user)
        this.user_id = data.user.id
        this.role_type = data.user.role_type
      }
    )
  }

  ngAfterViewInit() {
    Promise.resolve().then(() => {

      if (this.role_type == UserTypes.EMPLOYEE)
        this.dataSource?.loadWorkPlan(0, [{lop:"AND", col:"work_plans.user_id", cop:"=", val: this.user_id}], '', 'asc', 1, 10);
      else
        this.dataSource?.loadWorkPlan(0, [], '', 'asc', 1, 10);

      this.sort?.sortChange.subscribe(() => this.paginator.pageIndex = 0);

      merge(this.sort?.sortChange, this.paginator?.page)
        .pipe(
          tap(() => this.loadWorkPlanPage())
        )
        .subscribe();
    });
  }

  loadWorkPlanPage() {
    this.dataSource?.loadWorkPlan(
      0,
      [
        // { "col": "crc", "op" : "like", "val": this.filterCrc?.nativeElement.value},
        // { "col": "name", "op" : "like", "val": this.filterName?.nativeElement.value}
      ],
      this.sort.active,
      this.sort.direction,
      this.paginator.pageIndex + 1,
      this.paginator.pageSize);
  }

  onAddWorkPlan() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.width = "100%";
    dialogConfig.height = "auto";
    dialogConfig.autoFocus = false;
    dialogConfig.data = {};
    const dialogRef = this.dialog.open(WorkPlanAddUpdateComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(
      data => { }
    );
  }

  onEditWorkPlan(workPlanId: number) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.width = "100%";
    dialogConfig.height = "auto";
    dialogConfig.autoFocus = false;
    dialogConfig.data = {
      workPlanId: workPlanId
    };
    const dialogRef = this.dialog.open(WorkPlanAddUpdateComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(
      data => { }
    );
  }

}
