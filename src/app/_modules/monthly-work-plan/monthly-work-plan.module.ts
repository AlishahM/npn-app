import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MonthlyWorkPlanRoutingModule } from './monthly-work-plan-routing.module';
import { MonthlyWorkPlanComponent } from './monthly-work-plan.component';
import { WorkPlanAddUpdateComponent } from './work-plan-add-update/work-plan-add-update.component';
import { WorkPlanListComponent } from './work-plan-list/work-plan-list.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSelectModule } from '@angular/material/select';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [
    MonthlyWorkPlanComponent,
    WorkPlanAddUpdateComponent,
    WorkPlanListComponent
  ],
  imports: [
    CommonModule,
    MonthlyWorkPlanRoutingModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatPaginatorModule,
    MatSortModule,
    MatListModule,
    MatTableModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    SharedModule,
    MatSelectModule
  ]
})
export class MonthlyWorkPlanModule { }
