import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MonthlyWorkPlanComponent } from './monthly-work-plan.component';
import { WorkPlanAddUpdateComponent } from './work-plan-add-update/work-plan-add-update.component';
import { WorkPlanListComponent } from './work-plan-list/work-plan-list.component';

const routes: Routes = [{
  path: '', component: MonthlyWorkPlanComponent, children: [
    { path: 'work-plan-list', component: WorkPlanListComponent },
    { path: 'add-work-plan', component: WorkPlanAddUpdateComponent },
    { path: '', redirectTo: 'work-plan-list' }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MonthlyWorkPlanRoutingModule { }
