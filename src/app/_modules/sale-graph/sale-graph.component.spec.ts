import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SaleGraphComponent } from './sale-graph.component';

describe('SaleGraphComponent', () => {
  let component: SaleGraphComponent;
  let fixture: ComponentFixture<SaleGraphComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SaleGraphComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SaleGraphComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
