import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SaleGraphListComponent } from './sale-graph-list.component';

describe('SaleGraphListComponent', () => {
  let component: SaleGraphListComponent;
  let fixture: ComponentFixture<SaleGraphListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SaleGraphListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SaleGraphListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
