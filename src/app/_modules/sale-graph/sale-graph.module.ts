import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SaleGraphRoutingModule } from './sale-graph-routing.module';
import { SaleGraphComponent } from './sale-graph.component';
import { SaleGraphListComponent } from './sale-graph-list/sale-graph-list.component';
import { MatListModule } from '@angular/material/list';
import { ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { SharedModule } from '../shared/shared.module';
import { MatSelectModule } from '@angular/material/select';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { NgxEchartsModule } from 'ngx-echarts';


@NgModule({
  declarations: [
    SaleGraphComponent,
    SaleGraphListComponent
  ],
  imports: [
    CommonModule,
    SaleGraphRoutingModule,
    MatListModule,
    ReactiveFormsModule,
    MatInputModule,
    SharedModule,
    MatSelectModule,
    MatProgressSpinnerModule,
    NgxEchartsModule.forRoot({
      echarts: () => import('echarts'),
    }),
  ]
})
export class SaleGraphModule { }
