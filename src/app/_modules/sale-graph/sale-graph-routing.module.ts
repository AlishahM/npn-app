import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SaleGraphListComponent } from './sale-graph-list/sale-graph-list.component';
import { SaleGraphComponent } from './sale-graph.component';

const routes: Routes = [{ path: '', component: SaleGraphComponent, children: [
  { path: 'sale-graph-list', component: SaleGraphListComponent },
  { path: '', redirectTo: 'sale-graph-list' }
] }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SaleGraphRoutingModule { }
