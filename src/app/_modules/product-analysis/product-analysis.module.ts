import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductAnalysisRoutingModule } from './product-analysis-routing.module';
import { ProductAnalysisComponent } from './product-analysis.component';
import { ProductAnalysisListComponent } from './product-analysis-list/product-analysis-list.component';
import { ProductAnalysisChartComponent } from './product-analysis-chart/product-analysis-chart.component';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatListModule } from '@angular/material/list';
import { ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { SharedModule } from '../shared/shared.module';
import { MatSelectModule } from '@angular/material/select';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { NgxEchartsModule } from 'ngx-echarts';


@NgModule({
  declarations: [
    ProductAnalysisComponent,
    ProductAnalysisListComponent,
    ProductAnalysisChartComponent,
  ],
  imports: [
    CommonModule,
    ProductAnalysisRoutingModule,
    MatPaginatorModule,
    MatListModule,
    ReactiveFormsModule,
    MatInputModule,
    SharedModule,
    MatSelectModule,
    MatProgressSpinnerModule,
    NgxEchartsModule.forRoot({
      echarts: () => import('echarts'),
    }),
  ]
})
export class ProductAnalysisModule { }
