import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductAnalysisListComponent } from './product-analysis-list/product-analysis-list.component';
import { ProductAnalysisComponent } from './product-analysis.component';

const routes: Routes = [{ path: '', component: ProductAnalysisComponent, children: [
  { path: 'product-analysis-list', component: ProductAnalysisListComponent },
  { path: '', redirectTo: 'product-analysis-list' }
] }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductAnalysisRoutingModule { }
