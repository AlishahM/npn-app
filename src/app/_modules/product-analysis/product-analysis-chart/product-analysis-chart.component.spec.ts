import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductAnalysisChartComponent } from './product-analysis-chart.component';

describe('ProductAnalysisChartComponent', () => {
  let component: ProductAnalysisChartComponent;
  let fixture: ComponentFixture<ProductAnalysisChartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProductAnalysisChartComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductAnalysisChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
