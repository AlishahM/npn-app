import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { EChartsOption } from 'echarts';
import { Observable } from 'rxjs';
import { Company, UserTypes, Zone } from 'src/app/_models';
import { CompanyService, ZoneService } from 'src/app/_services';
import { ChartService } from 'src/app/_services/chart/chart.service';
import { UserService } from 'src/app/_services/user/user.service';

@Component({
  selector: 'app-product-analysis-chart',
  templateUrl: './product-analysis-chart.component.html',
  styleUrls: ['./product-analysis-chart.component.scss']
})
export class ProductAnalysisChartComponent implements OnInit {

  chartData: any;
  errors: any[] = [];

  form = new FormGroup({
    company_id: new FormControl('', [Validators.required]),
    zone_id: new FormControl('', [Validators.required]),
    employee_id: new FormControl('', [Validators.required]),
    year: new FormControl('', [Validators.required])
  });

  lookupCompanyObserver: Observable<any[]> | null | undefined;
  lookupCompanies: Company[] = [];
  lookupZoneObserver: Observable<any[]> | null | undefined;
  lookupZones: Zone[] = [];

  lookupUsers:any[] = [];
  
  chartOption: EChartsOption = {};

  constructor(
    private chartService: ChartService,
    private companyService: CompanyService,
    private zoneService: ZoneService,
    private userService: UserService
  ) { }

  ngOnInit(): void {
    this.loadCompanies();
    this.loadZones();
  }

  loadCompanies() {
    this.lookupCompanyObserver = this.companyService.findAll(0, [], '', '', -1);
    this.lookupCompanyObserver.subscribe(
      data => {
        this.lookupCompanies = data;
      },
      err => {
        console.log(err)
      }
    )
  }

  loadZones() {
    this.lookupZoneObserver = this.zoneService.findAll(0, [], '', '', -1);
    this.lookupZoneObserver.subscribe(
      data => {
        this.lookupZones = data;
      },
      err => {
        console.log(err)
      }
    )
  }

  loadUsers() {
    if (this.company_id?.value && this.zone_id?.value) {
      this.userService.findAll(
        [
          { lop: "AND", col:"users.company_id", cop:"=", val:this.company_id.value },
          { lop: "AND", col:"users.zone_id", cop:"=", val:this.zone_id.value },
          { lop: "AND", col:"users.role_type", cop:"=", val:UserTypes.EMPLOYEE }
        ],
        '',
        '',
        -1
      ).subscribe(
        data => {
          this.lookupUsers = data;
        },
        err => {
          this.errors = err.errors;
        }
      )
    }
  }

  loadChart() {
    if (this.company_id?.value && this.zone_id?.value && this.employee_id?.value && this.year?.value) {
      this.chartService.getProductAnalysisByTarget(
        this.company_id.value, this.zone_id.value, this.year.value, this.employee_id.value
      ).subscribe(
        data => {
          this.chartData = data
          this.displayChart()
        }
      )
    }
  }

  displayChart() {
    this.chartOption = {
      title: {
        text: 'Employee Incentive Report'
      },
      tooltip: {
        trigger: 'axis'
      },
      legend: {
        data: ['Target Sales', 'Actual Sale']
      },
      grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
      },
      toolbox: {
        feature: {
          saveAsImage: {}
        }
      },
      xAxis: {
        type: 'category',
        boundaryGap: false,
        data: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
      },
      yAxis: {
        type: 'value'
      },
      series: [
        {
          name: 'Target Sales',
          type: 'line',
          data: this.chartData?.target_assignment
        },
        {
          name: 'Actual Sale',
          type: 'line',
          data: this.chartData?.sale_expense
        }
      ]
    };
  }
  
  get company_id() {
    return this.form.get("company_id");
  }
  get zone_id() {
    return this.form.get("zone_id");
  }
  get year() {
    return this.form.get("year");
  }
  get employee_id() {
    return this.form.get("employee_id");
  }

}
