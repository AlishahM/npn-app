import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductAnalysisListComponent } from './product-analysis-list.component';

describe('ProductAnalysisListComponent', () => {
  let component: ProductAnalysisListComponent;
  let fixture: ComponentFixture<ProductAnalysisListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProductAnalysisListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductAnalysisListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
