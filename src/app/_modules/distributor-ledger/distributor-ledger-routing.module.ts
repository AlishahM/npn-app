import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DistributorLedgerListComponent } from './distributor-ledger-list/distributor-ledger-list.component';
import { DistributorLedgerComponent } from './distributor-ledger.component';

const routes: Routes = [{ path: '', component: DistributorLedgerComponent, children: [
  { path: 'distributor-ledger-list', component: DistributorLedgerListComponent },
  { path: '', redirectTo: 'distributor-ledger-list' }
] }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DistributorLedgerRoutingModule { }
