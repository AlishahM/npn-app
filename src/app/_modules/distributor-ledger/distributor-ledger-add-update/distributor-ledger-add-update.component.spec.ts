import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DistributorLedgerAddUpdateComponent } from './distributor-ledger-add-update.component';

describe('DistributorLedgerAddUpdateComponent', () => {
  let component: DistributorLedgerAddUpdateComponent;
  let fixture: ComponentFixture<DistributorLedgerAddUpdateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DistributorLedgerAddUpdateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DistributorLedgerAddUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
