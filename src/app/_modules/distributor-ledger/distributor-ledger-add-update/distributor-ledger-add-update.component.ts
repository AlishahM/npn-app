import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { validateAllFormFields } from 'src/app/_helpers';
import { DistributorLedgerService } from 'src/app/_services/distributor-ledger/distributor-ledger.service';

@Component({
  selector: 'app-distributor-ledger-add-update',
  templateUrl: './distributor-ledger-add-update.component.html',
  styleUrls: ['./distributor-ledger-add-update.component.scss']
})
export class DistributorLedgerAddUpdateComponent implements OnInit {

  loading: boolean = false;
  updated: boolean = false;
  errors: any;

  // Initializing Form with fields
  ledgerForm = new FormGroup({
    id: new FormControl(null, []),
    zone: new FormControl('', [Validators.required]),
    distributorName: new FormControl('', [Validators.required]),
    quantity: new FormControl('', [Validators.required]),
    totalAmount: new FormControl('', [Validators.required]),
    dopr: new FormControl('', [Validators.required]),
    status: new FormControl('', [Validators.required]),
  });

  constructor(
    public dialogRef: MatDialogRef<DistributorLedgerAddUpdateComponent>,
    private distributorLedgerService: DistributorLedgerService
  ) { }

  ngOnInit(): void {
  }

  onSubmit() {
    if (this.ledgerForm.valid) {
      this.distributorLedgerService.store(this.ledgerForm.value).subscribe(
        data => {
          console.log(data)
          this.onClose()
        },
        err => {
          console.log(err)
          this.errors = err.errors
        }
      )
    } else
      validateAllFormFields(this.ledgerForm);
  }

  onClear(): void {
    this.ledgerForm.reset()
  }

  onClose() {
    this.dialogRef.close({ updated: this.updated })
  }

  // Getters for form fields
  get zone() {
    return this.ledgerForm.get('zone')
  }
  get distributorName() {
    return this.ledgerForm.get('distributorName')
  }
  get quantity() {
    return this.ledgerForm.get('quantity')
  }
  get totalAmount() {
    return this.ledgerForm.get('totalAmount')
  }
  get dopr() {
    return this.ledgerForm.get('dopr')
  }
  get status() {
    return this.ledgerForm.get('status')
  }

}
