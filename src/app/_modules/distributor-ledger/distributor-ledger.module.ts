import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DistributorLedgerRoutingModule } from './distributor-ledger-routing.module';
import { DistributorLedgerComponent } from './distributor-ledger.component';
import { DistributorLedgerListComponent } from './distributor-ledger-list/distributor-ledger-list.component';
import { DistributorLedgerAddUpdateComponent } from './distributor-ledger-add-update/distributor-ledger-add-update.component';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSortModule } from '@angular/material/sort';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { MatListModule } from '@angular/material/list';
import { ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { SharedModule } from '../shared/shared.module';
import { MatSelectModule } from '@angular/material/select';


@NgModule({
  declarations: [
    DistributorLedgerComponent,
    DistributorLedgerListComponent,
    DistributorLedgerAddUpdateComponent
  ],
  imports: [
    CommonModule,
    DistributorLedgerRoutingModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatPaginatorModule,
    MatSortModule,
    MatListModule,
    MatTableModule,
    ReactiveFormsModule,
    MatInputModule,
    SharedModule,
    MatSelectModule
  ]
})
export class DistributorLedgerModule { }
