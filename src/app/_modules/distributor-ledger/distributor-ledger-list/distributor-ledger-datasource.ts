import { CollectionViewer, DataSource } from "@angular/cdk/collections";
import { Observable, BehaviorSubject, of } from "rxjs";
import { catchError, finalize } from "rxjs/operators";
import { DistributorLedgerService } from "src/app/_services/distributor-ledger/distributor-ledger.service";

export class DistributorLedgerDatasource implements DataSource<any> {

    totalItems = 0;

    private distributorLedgerSubject = new BehaviorSubject<any[]>([]);

    private loadingSubject = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingSubject.asObservable();

    constructor(private distributorLedgerService: DistributorLedgerService) { }

    loadDistributorLedger(courseId: number,
        filters: any[],
        sortBy: string,
        sortDirection: string,
        pageNumber: number,
        pageSize: number) {

        this.loadingSubject.next(true);
        this.distributorLedgerService.findAll(courseId, filters, sortBy, sortDirection,
            pageNumber, pageSize).pipe(
                catchError(() => of([])),
                finalize(() => this.loadingSubject.next(false))
            )
            .subscribe(data => {
                this.totalItems = data.total;
                return this.distributorLedgerSubject.next(data.data);
            });

    }

    connect(collectionViewer: CollectionViewer): Observable<any[]> {
        // bind observer
        console.log("Connecting data source");
        return this.distributorLedgerSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.distributorLedgerSubject.complete();
        this.loadingSubject.complete();
    }

}

