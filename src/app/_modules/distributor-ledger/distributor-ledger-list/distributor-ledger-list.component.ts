import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { merge } from 'rxjs';
import { tap } from 'rxjs/operators';
import { DistributorLedgerService } from 'src/app/_services/distributor-ledger/distributor-ledger.service';
import { DistributorLedgerAddUpdateComponent } from '../distributor-ledger-add-update/distributor-ledger-add-update.component';
import { DistributorLedgerDatasource } from './distributor-ledger-datasource';

@Component({
  selector: 'app-distributor-ledger-list',
  templateUrl: './distributor-ledger-list.component.html',
  styleUrls: ['./distributor-ledger-list.component.scss']
})
export class DistributorLedgerListComponent implements OnInit {

  dataSource: DistributorLedgerDatasource | any;

  displayedColumns = ["id", "zone", "distributorName", "quantity", "totalAmount", "dopr", "status"];

  @ViewChild(MatPaginator) paginator: MatPaginator | any;
  @ViewChild(MatSort)
  sort: MatSort = new MatSort;

  constructor(private dialog: MatDialog, private distributorLedgerService: DistributorLedgerService) { }

  ngOnInit(): void {
    this.dataSource = new DistributorLedgerDatasource(this.distributorLedgerService);
  }

  ngAfterViewInit() {
    Promise.resolve().then(() => {

      this.dataSource?.loadDistributorLedger(0, [], '', 'asc', 1, 10);

      this.sort?.sortChange.subscribe(() => this.paginator.pageIndex = 0);

      merge(this.sort?.sortChange, this.paginator?.page)
        .pipe(
          tap(() => this.loadDistributorLedger())
        )
        .subscribe();
    });
  }

  loadDistributorLedger() {
    console.log(this.paginator?.pageIndex)
    this.dataSource?.loadDistributorLedger(
      0,
      [
        // { "col": "crc", "op" : "like", "val": this.filterCrc?.nativeElement.value},
        // { "col": "name", "op" : "like", "val": this.filterName?.nativeElement.value}
      ],
      this.sort?.active,
      this.sort?.direction,
      this.paginator?.pageIndex + 1,
      this.paginator?.pageSize);
  }

  onDistributorLedger() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.width = "930px";
    dialogConfig.autoFocus = false;
    dialogConfig.data = {};
    const dialogRef = this.dialog.open(DistributorLedgerAddUpdateComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(
      data => {
        this.loadDistributorLedger()
      }
    );
  }

}
