import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DistributorLedgerListComponent } from './distributor-ledger-list.component';

describe('DistributorLedgerListComponent', () => {
  let component: DistributorLedgerListComponent;
  let fixture: ComponentFixture<DistributorLedgerListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DistributorLedgerListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DistributorLedgerListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
