import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DistributorLedgerComponent } from './distributor-ledger.component';

describe('DistributorLedgerComponent', () => {
  let component: DistributorLedgerComponent;
  let fixture: ComponentFixture<DistributorLedgerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DistributorLedgerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DistributorLedgerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
