import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { merge } from 'rxjs';
import { tap } from 'rxjs/operators';
import { UserTypes } from 'src/app/_models';
import { AuthenticationService } from 'src/app/_services';
import { WorkPlanService } from 'src/app/_services/work-plan/work-plan.service';
import { WorkPlanDatasource } from '../../monthly-work-plan/work-plan-list/work-plan-datasource';

@Component({
  selector: 'app-attendance-list',
  templateUrl: './attendance-list.component.html',
  styleUrls: ['./attendance-list.component.scss']
})
export class AttendanceListComponent implements OnInit {

  dataSource: WorkPlanDatasource | any;
  user_id?: number;
  role_type?: number;

  displayedColumns = ["plan_no", "user.name", "from_date", "to_date", "month", "year", "actions"];

  @ViewChild(MatPaginator) paginator: MatPaginator | any;
  @ViewChild(MatSort)
  sort: MatSort = new MatSort;

  constructor(
    private workPlanService: WorkPlanService,
    private authenticationService: AuthenticationService,
  ) { }

  ngOnInit(): void {
    this.dataSource = new WorkPlanDatasource(this.workPlanService);
    this.authenticationService.currentEmployee.subscribe(
      (data: any) => {
        console.log(data.user)
        this.user_id = data.user.id
        this.role_type = data.user.role_type
      }
    )
  }

  ngAfterViewInit() {
    Promise.resolve().then(() => {

      if (this.role_type == UserTypes.EMPLOYEE)
        this.dataSource?.loadWorkPlan(0, [{lop:"AND", col:"work_plans.user_id", cop:"=", val: this.user_id}], '', 'asc', 1, 10);
      else
        this.dataSource?.loadWorkPlan(0, [], '', 'asc', 1, 10);

      this.sort?.sortChange.subscribe(() => this.paginator.pageIndex = 0);

      merge(this.sort?.sortChange, this.paginator?.page)
        .pipe(
          tap(() => this.loadWorkPlanPage())
        )
        .subscribe();
    });
  }

  loadWorkPlanPage() {
    this.dataSource?.loadWorkPlan(
      0,
      [
        // { "col": "crc", "op" : "like", "val": this.filterCrc?.nativeElement.value},
        // { "col": "name", "op" : "like", "val": this.filterName?.nativeElement.value}
      ],
      this.sort.active,
      this.sort.direction,
      this.paginator.pageIndex + 1,
      this.paginator.pageSize);
  }

  updateAttendance(event: any, workplanId: number) {
    console.log(event.checked, workplanId)
    let jsonObject = {
      isPresent: event.checked,
      id: workplanId
    }
    this.workPlanService.update(jsonObject).subscribe(
      (data) => {
        console.log(data)
      }
    )
  }

}
