import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AttendanceListComponent } from './attendance-list/attendance-list.component';
import { AttendanceComponent } from './attendance.component';

const routes: Routes = [{ path: '', component: AttendanceComponent, children: [
  { path: 'attendance-list', component: AttendanceListComponent },
  { path: '', redirectTo: 'attendance-list' }
] }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AttendanceRoutingModule { }
