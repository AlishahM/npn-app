import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ClientInvestmentLedgerRoutingModule } from './client-investment-ledger-routing.module';
import { ClientInvestmentLedgerComponent } from './client-investment-ledger.component';
import { ClientInvestmentLedgerListComponent } from './client-investment-ledger-list/client-investment-ledger-list.component';
import { ClientInvestmentLedgerAddUpdateComponent } from './client-investment-ledger-add-update/client-investment-ledger-add-update.component';


@NgModule({
  declarations: [
    ClientInvestmentLedgerComponent,
    ClientInvestmentLedgerListComponent,
    ClientInvestmentLedgerAddUpdateComponent
  ],
  imports: [
    CommonModule,
    ClientInvestmentLedgerRoutingModule
  ]
})
export class ClientInvestmentLedgerModule { }
