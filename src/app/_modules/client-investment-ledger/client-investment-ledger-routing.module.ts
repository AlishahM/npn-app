import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ClientInvestmentLedgerAddUpdateComponent } from './client-investment-ledger-add-update/client-investment-ledger-add-update.component';
import { ClientInvestmentLedgerListComponent } from './client-investment-ledger-list/client-investment-ledger-list.component';
import { ClientInvestmentLedgerComponent } from './client-investment-ledger.component';

const routes: Routes = [{ path: '', component: ClientInvestmentLedgerComponent, children: [
  { path: 'client-investment-ledger-list', component: ClientInvestmentLedgerListComponent },
  { path: 'client-investment-ledger-add-update', component: ClientInvestmentLedgerAddUpdateComponent },
  { path: '', redirectTo: 'client-investment-ledger-list' }
] }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClientInvestmentLedgerRoutingModule { }
