import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientInvestmentLedgerAddUpdateComponent } from './client-investment-ledger-add-update.component';

describe('ClientInvestmentLedgerAddUpdateComponent', () => {
  let component: ClientInvestmentLedgerAddUpdateComponent;
  let fixture: ComponentFixture<ClientInvestmentLedgerAddUpdateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ClientInvestmentLedgerAddUpdateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientInvestmentLedgerAddUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
