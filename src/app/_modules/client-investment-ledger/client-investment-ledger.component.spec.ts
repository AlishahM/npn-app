import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientInvestmentLedgerComponent } from './client-investment-ledger.component';

describe('ClientInvestmentLedgerComponent', () => {
  let component: ClientInvestmentLedgerComponent;
  let fixture: ComponentFixture<ClientInvestmentLedgerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ClientInvestmentLedgerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientInvestmentLedgerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
