import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientInvestmentLedgerListComponent } from './client-investment-ledger-list.component';

describe('ClientInvestmentLedgerListComponent', () => {
  let component: ClientInvestmentLedgerListComponent;
  let fixture: ComponentFixture<ClientInvestmentLedgerListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ClientInvestmentLedgerListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientInvestmentLedgerListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
