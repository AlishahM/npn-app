import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StockAnalysisListComponent } from './stock-analysis-list/stock-analysis-list.component';
import { StockAnalysisComponent } from './stock-analysis.component';

const routes: Routes = [{ path: '', component: StockAnalysisComponent, children: [
  { path: 'stock-analysis-list', component: StockAnalysisListComponent },
  { path: '', redirectTo: 'stock-analysis-list' }
] }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StockAnalysisRoutingModule { }
