import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StockAnalysisListComponent } from './stock-analysis-list.component';

describe('StockAnalysisListComponent', () => {
  let component: StockAnalysisListComponent;
  let fixture: ComponentFixture<StockAnalysisListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StockAnalysisListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StockAnalysisListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
