import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StockAnalysisRoutingModule } from './stock-analysis-routing.module';
import { StockAnalysisComponent } from './stock-analysis.component';
import { StockAnalysisListComponent } from './stock-analysis-list/stock-analysis-list.component';


@NgModule({
  declarations: [
    StockAnalysisComponent,
    StockAnalysisListComponent
  ],
  imports: [
    CommonModule,
    StockAnalysisRoutingModule
  ]
})
export class StockAnalysisModule { }
