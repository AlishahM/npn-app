import { DatePipe } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { Company, Products, UserTypes } from 'src/app/_models';
import { AuthenticationService, CompanyService, ProductsService } from 'src/app/_services';
import { PurchaseOrderService } from 'src/app/_services/purchase-order/purchase-order.service';

@Component({
  selector: 'app-purchase-order-add-update',
  templateUrl: './purchase-order-add-update.component.html',
  styleUrls: ['./purchase-order-add-update.component.scss']
})
export class PurchaseOrderAddUpdateComponent implements OnInit {
  
  lookupCompanyObserver: Observable<any[]> | null | undefined;
  lookupZones: Zone[] = [];
  lookupCompanies: Company[] = [];

  purchaseOrderForm = new FormGroup({
    id: new FormControl(null, []),
    user_id: new FormControl(null, []),
    role_type: new FormControl(null, []),
    zone_id: new FormControl(null, []),
    distributer_area: new FormControl(null, []),
    order_date: new FormControl(new Date, [Validators.required]),
    company_id: new FormControl('', [Validators.required]),
    is_accepted: new FormControl(null, []),
    product_units: new FormArray([]),
  });

  loading: boolean = false;
  updated: boolean = false;
  updating: boolean = false;
  isEditMode: boolean = false;
  errors: any;
  currentEmployee: any;
  employee: any;
  products: Products[] = [];

  purchaseOrderId?: number;
  primary:string = "primary";
  UserTypes = UserTypes
  total_order_unit: number = 0
  total_price: number = 0

  constructor(
    public dialogRef: MatDialogRef<PurchaseOrderAddUpdateComponent>,
    private purchaseOrderService: PurchaseOrderService,
    private authenticationService: AuthenticationService,
    private productService: ProductsService,
    private companyService: CompanyService,
    @Inject(MAT_DIALOG_DATA) data: any
  ) {
    if (data.purchaseOrderId) {
      this.isEditMode = true;
      this.purchaseOrderId = data.purchaseOrderId
    }
  }

  ngOnInit(): void {
    this.loading = true
    this.loadCompanies();
    if (this.isEditMode) {
      this.getPurchaseOrderById();
      this.authenticationService.currentEmployee.subscribe(
        (data: any) => {
          console.log(data.user)
          this.employee = data.user
        });
    } else {
      this.authenticationService.currentEmployee.subscribe(
        (data: any) => {
          console.log(data.user)
          this.currentEmployee = data.user
          this.user_id?.setValue(data.user.id)
          this.role_type?.setValue(data.user.role_type)
          this.zone_id?.setValue(data.user.zone_id)
          this.distributer_area?.setValue(data.user.zone.area_name)
          this.company_id?.setValue(data.user.company_id)
          this.loadProducts(data.user.company_id)
        }
      )
    }
  }

  getPurchaseOrderById() {
    this.purchaseOrderService.find(this.purchaseOrderId).subscribe(
      data => {
        this.purchaseOrderForm.get("id")?.setValue(data.id)
        this.currentEmployee = data.user
        this.user_id?.setValue(data.user_id)
        this.role_type?.setValue(data.user.role_type)
        this.zone_id?.setValue(data.zone_id)
        this.distributer_area?.setValue(data.distributer_area)
        this.company_id?.setValue(+data.company_id)
        this.order_date?.setValue(data.order_date)
        this.onAddProductUnitByPO(data.product_units)
        this.is_accepted?.setValue(data.is_accepted)
        this.loading = false;
        this.countOrderUnit();
      }
    )
  }

  loadCompanies() {
    this.lookupCompanyObserver = this.companyService.findAll(0, [], '', '', -1);
    this.lookupCompanyObserver.subscribe(
      (data: any) => {
        this.lookupCompanies = data;
      },
      (err: any) => {
        console.log(err)
      }
    )
  }

  loadProducts(company_id: number) {
    this.productService.findAll(0, [
      { lop: "AND", col: "products.company_id", cop: "=", val: company_id }
    ], '', '', -1).subscribe(
      data => {
        console.log(data);
        this.products = data;
        this.onAddProductUnit(data)
        this.loading = false;
      }
    )
  }

  onAddProductUnit(productUnit: any) {
    productUnit.forEach((element: any) => {
      const itemGrp = new FormGroup({
        id: new FormControl("", []),
        product_id: new FormControl(element.id, [Validators.required]),
        product_name: new FormControl(element.name, [Validators.required]),
        price: new FormControl(element.price, [Validators.required]),
        order_unit: new FormControl(element.order_unit, [Validators.required])
      });
      // itemGrp.patchValue(element, { onlySelf: true });
      this.product_units.push(itemGrp);
    });
    
  }

  onAddProductUnitByPO(productUnit: any) {
    productUnit.forEach((element: any) => {
      const itemGrp = new FormGroup({
        id: new FormControl(element.id, []),
        product_id: new FormControl(element.product_id, [Validators.required]),
        product_name: new FormControl(element.product_name, [Validators.required]),
        price: new FormControl(element.price, [Validators.required]),
        order_unit: new FormControl(element.order_unit, [Validators.required])
      });
      // itemGrp.patchValue(element, { onlySelf: true });
      this.product_units.push(itemGrp);
    });
  }

  onSubmit() {
    console.log(this.purchaseOrderForm.valid)
    if (this.purchaseOrderForm.valid) {
      this.updating = true;
      if (!this.isEditMode) {
        this.is_accepted?.setValue(null)
        this.purchaseOrderService.store(this.purchaseOrderForm.value).subscribe(
          data => {
            this.updating = false;
            this.updated = true;
            this.onClose();
          },
          err => {
            this.updating = false;
            this.errors = err.errors;
          }
        )
      } else {
        this.purchaseOrderService.update(this.purchaseOrderForm.value).subscribe(
          data => {
            this.updating = false;
            this.updated = true;
            this.onClose();
          },
          err => {
            this.updating = false;
            this.errors = err.errors;
          }
        )
      }
    }
  }

  countOrderUnit() {
    let singleOrderUnit = 0, singleOrderPrice = 0;
    this.total_order_unit = 0;
    this.total_price = 0;
    this.product_units.controls.forEach((element, index) => {
      singleOrderUnit = +this.product_units.controls[index].get('order_unit')?.value
      if(singleOrderUnit == null)
        singleOrderUnit = 0

      singleOrderPrice = singleOrderUnit * +this.product_units.controls[index].get("price")?.value
      
      this.total_order_unit += singleOrderUnit
      this.total_price += singleOrderPrice
    });
  }

  onClose() {
    this.dialogRef.close({ updated: this.updated })
  }

  get user_id() {
    return this.purchaseOrderForm.get("user_id");
  }
  get role_type() {
    return this.purchaseOrderForm.get("role_type");
  }
  get zone_id() {
    return this.purchaseOrderForm.get("zone_id");
  }
  get distributer_area() {
    return this.purchaseOrderForm.get("distributer_area");
  }
  get order_date() {
    return this.purchaseOrderForm.get("order_date");
  }
  get company_id() {
    return this.purchaseOrderForm.get("company_id");
  }
  get is_accepted() {
    return this.purchaseOrderForm.get("is_accepted");
  }
  get product_units() {
    return this.purchaseOrderForm.controls["product_units"] as FormArray;
  }
}
