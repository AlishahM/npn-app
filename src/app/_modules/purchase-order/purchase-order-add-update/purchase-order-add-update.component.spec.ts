import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchaseOrderAddUpdateComponent } from './purchase-order-add-update.component';

describe('PurchaseOrderAddUpdateComponent', () => {
  let component: PurchaseOrderAddUpdateComponent;
  let fixture: ComponentFixture<PurchaseOrderAddUpdateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PurchaseOrderAddUpdateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchaseOrderAddUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
