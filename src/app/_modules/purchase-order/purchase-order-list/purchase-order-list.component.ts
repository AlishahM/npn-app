import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { merge } from 'rxjs';
import { tap } from 'rxjs/operators';
import { UserTypes } from 'src/app/_models';
import { AuthenticationService } from 'src/app/_services';
import { PurchaseOrderService } from 'src/app/_services/purchase-order/purchase-order.service';
import { PurchaseOrderAddUpdateComponent } from '../purchase-order-add-update/purchase-order-add-update.component';
import { PurchaseOrderDatasource } from './purchase-order-datasource';

@Component({
  selector: 'app-purchase-order-list',
  templateUrl: './purchase-order-list.component.html',
  styleUrls: ['./purchase-order-list.component.scss']
})
export class PurchaseOrderListComponent implements OnInit {

  dataSource: PurchaseOrderDatasource | any;
  user_id?: number;
  role_type?: number;

  displayedColumns = ["order_date", "po_no", "employeeName", "zone", "area", "company", "is_accepted","actions"];

  @ViewChild(MatPaginator) paginator: MatPaginator | any;
  @ViewChild(MatSort)
  sort: MatSort = new MatSort;

  @ViewChild('reload') reload: ElementRef | undefined;

  constructor(
    private purchaseOrderService: PurchaseOrderService,
    private authenticationService: AuthenticationService,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.dataSource = new PurchaseOrderDatasource(this.purchaseOrderService);
    this.authenticationService.currentEmployee.subscribe(
      (data: any) => {
        console.log(data.user)
        this.user_id = data.user.id
        this.role_type = data.user.role_type
      }
    )
  }

  ngAfterViewInit() {
    Promise.resolve().then(() => {

      if (this.role_type != UserTypes.ADMIN)
      this.dataSource?.loadPurchaseOrder(0, [{lop:"AND", col:"purchase_orders.user_id", cop:"=", val: this.user_id}], '', 'asc', 1, 10);
      else
      this.dataSource?.loadPurchaseOrder(0, [], '', 'asc', 1, 10);

      this.sort?.sortChange.subscribe(() => this.paginator.pageIndex = 0);

      merge(this.sort?.sortChange, this.paginator?.page)
        .pipe(
          tap(() => this.loadPurchaseOrderPage())
        )
        .subscribe();
    });
  }

  loadPurchaseOrderPage() {
    this.dataSource?.loadPurchaseOrder(
      0,
      this.role_type != UserTypes.ADMIN ? [{lop:"AND", col:"purchase_orders.user_id", cop:"=", val: this.user_id}]: [],
      this.sort.active,
      this.sort.direction,
      this.paginator.pageIndex + 1,
      this.paginator.pageSize);
  }

  onPurchaseOrder() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.minWidth = "75vw";
    dialogConfig.minHeight = "95vh";
    dialogConfig.autoFocus = false;
    dialogConfig.disableClose = true;
    dialogConfig.data = {};
    const dialogRef = this.dialog.open(PurchaseOrderAddUpdateComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(
      data => {
        if (data.updated)
        this.loadPurchaseOrderPage()
      }
    );
  }

  onPurchaseOrderView(purchaseOrderId: number) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.minWidth = "75vw";
    dialogConfig.minHeight = "95vh";
    dialogConfig.autoFocus = false;
    dialogConfig.disableClose = true;
    dialogConfig.data = {
      purchaseOrderId: purchaseOrderId
    };
    const dialogRef = this.dialog.open(PurchaseOrderAddUpdateComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(
      data => {
        if (data.updated)
        this.loadPurchaseOrderPage()
      }
    );
  }
}
