import { CollectionViewer, DataSource } from "@angular/cdk/collections";
import { Observable, BehaviorSubject, of } from "rxjs";
import { catchError, finalize } from "rxjs/operators";
import { PurchaseOrderService } from "src/app/_services/purchase-order/purchase-order.service";

export class PurchaseOrderDatasource implements DataSource<any> {

    totalItems = 0;

    private purchaseOrderSubject = new BehaviorSubject<any[]>([]);

    private loadingSubject = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingSubject.asObservable();

    constructor(private purchaseOrderService: PurchaseOrderService) { }

    loadPurchaseOrder(purchaseOrderId: number,
        filters: any[],
        sortBy: string,
        sortDirection: string,
        pageNumber: number,
        pageSize: number) {

        this.loadingSubject.next(true);
        this.purchaseOrderService.findAll(purchaseOrderId, filters, sortBy, sortDirection,
            pageNumber, pageSize).pipe(
                catchError(() => of([])),
                finalize(() => this.loadingSubject.next(false))
            )
            .subscribe((data:any) => {
                this.totalItems = data.total;
                return this.purchaseOrderSubject.next(data.data);
            });

    }

    connect(collectionViewer: CollectionViewer): Observable<any[]> {
        // bind observer
        console.log("Connecting data source");
        return this.purchaseOrderSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.purchaseOrderSubject.complete();
        this.loadingSubject.complete();
    }

}

