import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PurchaseOrderAddUpdateComponent } from './purchase-order-add-update/purchase-order-add-update.component';
import { PurchaseOrderDetailComponent } from './purchase-order-detail/purchase-order-detail.component';
import { PurchaseOrderListComponent } from './purchase-order-list/purchase-order-list.component';
import { PurchaseOrderComponent } from './purchase-order.component';

const routes: Routes = [{ 
  path: '', component: PurchaseOrderComponent, children: [
    { path: 'purchase-order-list', component: PurchaseOrderListComponent },
    { path: 'add-purchase-order', component: PurchaseOrderAddUpdateComponent },
    { path: 'purchase-order-detail', component: PurchaseOrderDetailComponent },
    { path: '', redirectTo: 'purchase-order-list' }
  ]
 }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PurchaseOrderRoutingModule { }
