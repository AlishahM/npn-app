import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesExpenseAddUpdateComponent } from './sales-expense-add-update.component';

describe('SalesExpenseAddUpdateComponent', () => {
  let component: SalesExpenseAddUpdateComponent;
  let fixture: ComponentFixture<SalesExpenseAddUpdateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SalesExpenseAddUpdateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesExpenseAddUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
