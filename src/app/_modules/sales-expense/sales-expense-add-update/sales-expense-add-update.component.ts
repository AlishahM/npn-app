import { Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { validateAllFormFields } from 'src/app/_helpers';
import { Products } from 'src/app/_models';
import { AuthenticationService, ProductsService, SalesExpenseService } from 'src/app/_services';

@Component({
  selector: 'app-sales-expense-add-update',
  templateUrl: './sales-expense-add-update.component.html',
  styleUrls: ['./sales-expense-add-update.component.scss']
})
export class SalesExpenseAddUpdateComponent implements OnInit {

  salesExpenseForm = new FormGroup({
    id: new FormControl(null, []),
    user_id: new FormControl(null, []),
    role_type: new FormControl(null, []),
    company_id: new FormControl(null, []),
    zone_id: new FormControl(null, []),
    total_unit_sold: new FormControl(null, []),
    total_sale_value: new FormControl(null, []),
    product_units: new FormArray([]),
    monthly_expense_details: new FormArray([]),
    annualy_expense_details: new FormArray([]),
    claim_expense_details: new FormArray([]),
  });
  monthlyExpenseForm = new FormGroup({
    monthlyDate: new FormControl(null, []),
    monthlyName: new FormControl(null, []),
    monthlyAmount: new FormControl(null, []),
  });
  annualyExpenseForm = new FormGroup({
    annualyDate: new FormControl(null, []),
    annualyName: new FormControl(null, []),
    annualyAmount: new FormControl(null, []),
  });
  claimExpenseForm = new FormGroup({
    claimDate: new FormControl(null, []),
    claimName: new FormControl(null, []),
    claimAmount: new FormControl(null, []),
  });

  loading: boolean = false;
  updated: boolean = false;
  errors: any;
  currentEmployee: any;
  products: Products[] = [];

  constructor(
    public dialogRef: MatDialogRef<SalesExpenseAddUpdateComponent>,
    private salesExpenseService: SalesExpenseService,
    private authenticationService: AuthenticationService,
    private productService: ProductsService,
    @Inject(MAT_DIALOG_DATA) data: any
  ) { }

  ngOnInit(): void {
    this.loading = true
    this.authenticationService.currentEmployee.subscribe(
      (data: any) => {
        console.log(data.user)
        this.currentEmployee = data.user
        this.user_id?.setValue(data.user.id)
        this.role_type?.setValue(data.user.role_type)
        this.company_id?.setValue(data.user.company_id)
        this.zone_id?.setValue(data.user.zone_id)
        this.loadProducts(data.user.company_id)
      }
    )
  }

  loadProducts(company_id: number) {
    this.productService.findAll(0, [
      { lop: "AND", col: "products.company_id", cop: "=", val: company_id }
    ], '', '', -1).subscribe(
      data => {
        console.log(data);
        this.products = data;
        this.onAddProductUnit(data)
        this.loading = false;
      }
    )
  }

  onSubmit(): void {
    if (this.salesExpenseForm.valid) {
      this.salesExpenseService.store(this.salesExpenseForm.value).subscribe(
        data => {
          console.log(data)
          this.onClose()
        },
        err => {
          console.log(err)
          this.errors = err.errors
        }
      )
    } else
      validateAllFormFields(this.salesExpenseForm);
  }

  onAddProductUnit(productUnit: any) {
    // if (!itemMovement) {
    //   isNew = true;
    //   itemMovement = new ItemMovementDetail();
    // } else {
    //   itemMovement = new ItemMovementDetail().deserialize(itemMovement)
    // }

    productUnit.forEach((element: any) => {
      console.log("onAddProductUnitItem().clicked()");
      const itemGrp = new FormGroup({
        id: new FormControl("", []),
        product_id: new FormControl(element.id, [Validators.required]),
        name: new FormControl(element.name, [Validators.required]),
        price: new FormControl(element.price, [Validators.required]),
        product_unit: new FormControl('', [Validators.required]),
        calculated_product_unit: new FormControl('', []),
      });
      // itemGrp.patchValue(element, { onlySelf: true });
      this.product_units.push(itemGrp);
    });
    
  }

  onAddMonthlyExpense() {
    const monthlyExpenseValue = this.monthlyExpenseForm.value;
    const itemGrp = new FormGroup({
      id: new FormControl("", []),
      date: new FormControl(this.monthlyDate?.value, [Validators.required]),
      name: new FormControl(this.monthlyName?.value, [Validators.required]),
      amount: new FormControl(this.monthlyAmount?.value, [Validators.required]),
    });
    this.monthly_expense_details.push(itemGrp);
    this.monthlyExpenseForm.reset()
  }

  onRemoveMonthlyExpense(index: number) {
    this.monthly_expense_details.removeAt(index);
  }

  onAddAnnualyExpense() {
    const annualyExpenseValue = this.annualyExpenseForm.value;
    const itemGrp = new FormGroup({
      id: new FormControl("", []),
      date: new FormControl(this.annualyDate?.value, [Validators.required]),
      name: new FormControl(this.annualyName?.value, [Validators.required]),
      amount: new FormControl(this.annualyAmount?.value, [Validators.required]),
    });
    this.annualy_expense_details.push(itemGrp);
    this.annualyExpenseForm.reset()
  }

  onRemoveAnnualyExpense(index: number) {
    this.annualy_expense_details.removeAt(index);
  }

  onAddClaimExpense() {
    const claimExpenseValue = this.claimExpenseForm.value;
    const itemGrp = new FormGroup({
      id: new FormControl("", []),
      date: new FormControl(this.claimDate?.value, [Validators.required]),
      name: new FormControl(this.claimName?.value, [Validators.required]),
      amount: new FormControl(this.claimAmount?.value, [Validators.required]),
    });
    this.claim_expense_details.push(itemGrp);
    this.claimExpenseForm.reset()
  }

  onRemoveClaimExpense(index: number) {
    this.annualy_expense_details.removeAt(index);
  }

  onTotalUnitsSold(): number {
    let units_sold: number = 0;
    this.product_units.controls.forEach((element: any) => {
      units_sold += +element.get("product_unit").value
    });
    this.total_unit_sold?.setValue(units_sold)
    return units_sold;
  }

  onTotalSaleValue(): number {
    let sale_value: number = 0;
    this.product_units.controls.forEach((element: any) => {
      sale_value += (element.get("price").value * element.get("product_unit").value)
    });
    this.total_sale_value?.setValue(sale_value)
    return sale_value;
  }

  onTotalMonthExpense(): number {
    let monthly_expense: number = 0;
    this.monthly_expense_details.controls.forEach((element: any) => {
      monthly_expense += (element.get("amount").value)
    });
    return monthly_expense;
  }

  onTotalAnnualExpense(): number {
    let annual_expense: number = 0;
    this.annualy_expense_details.controls.forEach((element: any) => {
      annual_expense += (element.get("amount").value)
    });
    return annual_expense;
  }

  onTotalCurrentClaimExpense(): number {
    let claim_expense: number = 0;
    this.claim_expense_details.controls.forEach((element: any) => {
      claim_expense += (element.get("amount").value)
    });
    return claim_expense;
  }

  onClear(): void {
    this.salesExpenseForm.reset()
  }

  onClose() {
    this.dialogRef.close({ updated: this.updated })
  }

  get user_id() {
    return this.salesExpenseForm.get("user_id");
  }
  get role_type() {
    return this.salesExpenseForm.get("role_type");
  }
  get company_id() {
    return this.salesExpenseForm.get("company_id");
  }
  get zone_id() {
    return this.salesExpenseForm.get("zone_id");
  }
  get total_unit_sold() {
    return this.salesExpenseForm.get("total_unit_sold");
  }
  get total_sale_value() {
    return this.salesExpenseForm.get("total_sale_value");
  }
  get product_units() {
    return this.salesExpenseForm.controls["product_units"] as FormArray;
  }
  get monthly_expense_details() {
    return this.salesExpenseForm.controls["monthly_expense_details"] as FormArray;
  }
  get annualy_expense_details() {
    return this.salesExpenseForm.controls["annualy_expense_details"] as FormArray;
  }
  get claim_expense_details() {
    return this.salesExpenseForm.controls["claim_expense_details"] as FormArray;
  }
  get monthlyDate() {
    return this.monthlyExpenseForm.get("monthlyDate");
  }
  get monthlyName() {
    return this.monthlyExpenseForm.get("monthlyName");
  }
  get monthlyAmount() {
    return this.monthlyExpenseForm.get("monthlyAmount");
  }
  get annualyDate() {
    return this.annualyExpenseForm.get("annualyDate");
  }
  get annualyName() {
    return this.annualyExpenseForm.get("annualyName");
  }
  get annualyAmount() {
    return this.annualyExpenseForm.get("annualyAmount");
  }
  get claimDate() {
    return this.claimExpenseForm.get("claimDate");
  }
  get claimName() {
    return this.claimExpenseForm.get("claimName");
  }
  get claimAmount() {
    return this.claimExpenseForm.get("claimAmount");
  }

}
