import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesExpenseComponent } from './sales-expense.component';

describe('SalesExpenseComponent', () => {
  let component: SalesExpenseComponent;
  let fixture: ComponentFixture<SalesExpenseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SalesExpenseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesExpenseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
