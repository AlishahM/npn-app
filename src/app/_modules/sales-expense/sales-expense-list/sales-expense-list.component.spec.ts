import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesExpenseListComponent } from './sales-expense-list.component';

describe('SalesExpenseListComponent', () => {
  let component: SalesExpenseListComponent;
  let fixture: ComponentFixture<SalesExpenseListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SalesExpenseListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesExpenseListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
