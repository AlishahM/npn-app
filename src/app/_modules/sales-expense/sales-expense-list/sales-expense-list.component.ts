import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { merge } from 'rxjs';
import { tap } from 'rxjs/operators';
import { SalesExpenseProductService } from 'src/app/_services';
import { SalesExpenseAddUpdateComponent } from '../sales-expense-add-update/sales-expense-add-update.component';
import { SalesExpenseDatasource } from './sales-expense-datasource';

@Component({
  selector: 'app-sales-expense-list',
  templateUrl: './sales-expense-list.component.html',
  styleUrls: ['./sales-expense-list.component.scss']
})
export class SalesExpenseListComponent implements OnInit {

  dataSource: SalesExpenseDatasource | any;

  displayedColumns = ["id", "sale_expense.user.zone.zone", "employee", "name", "month", "quantity", "total_price"];

  @ViewChild(MatPaginator) paginator: MatPaginator | any;
  @ViewChild(MatSort)
  sort: MatSort = new MatSort;

  @ViewChild('reload') reload: ElementRef | undefined;

  constructor(
    private salesExpenseProductService: SalesExpenseProductService,
    private dialog: MatDialog,
  ) { }

  ngOnInit(): void {
    this.dataSource = new SalesExpenseDatasource(this.salesExpenseProductService);
  }

  ngAfterViewInit() {
    Promise.resolve().then(() => {

      this.dataSource?.loadSalesExpense(0, [], '', 'asc', 1, 10);

      this.sort?.sortChange.subscribe(() => this.paginator.pageIndex = 0);

      merge(this.sort.sortChange, this.paginator.page)
        .pipe(
          tap(() => this.loadSalesExpensePage())
        )
        .subscribe();
    });
  }

  loadSalesExpensePage() {
    console.log('loadSalesExpensePage()');
    console.log(this.paginator.pageIndex)
    this.dataSource?.loadSalesExpense(
      0,
      [
        // { "col": "crc", "op" : "like", "val": this.filterCrc?.nativeElement.value},
        // { "col": "name", "op" : "like", "val": this.filterName?.nativeElement.value}
      ],
      this.sort.active,
      this.sort.direction,
      this.paginator.pageIndex + 1,
      this.paginator.pageSize);
  }

  onSalesExpense() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.minWidth = "75vw";
    dialogConfig.minHeight = "95vh";
    dialogConfig.autoFocus = false;
    dialogConfig.disableClose = true;
    dialogConfig.data = {};
    const dialogRef = this.dialog.open(SalesExpenseAddUpdateComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(
      data => {
        this.loadSalesExpensePage()
      }
    );
  }

}
