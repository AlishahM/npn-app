import { CollectionViewer, DataSource } from "@angular/cdk/collections";
import { Observable, BehaviorSubject, of } from "rxjs";
import { catchError, finalize } from "rxjs/operators";
import { SalesExpenseProductService } from "../../../_services";

export class SalesExpenseDatasource implements DataSource<any> {

    totalItems = 0;

    private saleExpenseSubject = new BehaviorSubject<any[]>([]);

    private loadingSubject = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingSubject.asObservable();

    constructor(private salesExpenseProductService: SalesExpenseProductService) { }

    loadSalesExpense(courseId: number,
        filters: any[],
        sortBy: string,
        sortDirection: string,
        pageNumber: number,
        pageSize: number) {

        this.loadingSubject.next(true);
        this.salesExpenseProductService.findAll(courseId, filters, sortBy, sortDirection,
            pageNumber, pageSize).pipe(
                catchError(() => of([])),
                finalize(() => this.loadingSubject.next(false))
            )
            .subscribe(data => {
                this.totalItems = data.total;
                return this.saleExpenseSubject.next(data.data);
            });

    }

    connect(collectionViewer: CollectionViewer): Observable<any[]> {
        // bind observer
        console.log("Connecting data source");
        return this.saleExpenseSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.saleExpenseSubject.complete();
        this.loadingSubject.complete();
    }

}

