import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesExpenseDetailComponent } from './sales-expense-detail.component';

describe('SalesExpenseDetailComponent', () => {
  let component: SalesExpenseDetailComponent;
  let fixture: ComponentFixture<SalesExpenseDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SalesExpenseDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesExpenseDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
