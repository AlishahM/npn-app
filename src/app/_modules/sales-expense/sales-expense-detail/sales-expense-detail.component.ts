import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { Company, UserTypes } from 'src/app/_models';
import { CompanyService, SalesExpenseService } from 'src/app/_services';

@Component({
  selector: 'app-sales-expense-detail',
  templateUrl: './sales-expense-detail.component.html',
  styleUrls: ['./sales-expense-detail.component.scss']
})
export class SalesExpenseDetailComponent implements OnInit {

  form = new FormGroup({
    company_id: new FormControl(null, [])
  })

  lookupCompanyObserver: Observable<any[]> | null | undefined;
  lookupCompanies: Company[] = [];

  id:any;
  loading: boolean = false;
  updated: boolean = false;
  companySelected: boolean = false;
  errors: any;
  employee_sales: any;
  employee_sales_sum: any[] = [];
  distributer_sales: any;
  distributer_sales_sum: any;

  constructor(
    private companyService: CompanyService,
    private salesExpenseService: SalesExpenseService,
  ) { }

  ngOnInit(): void {
    this.loadCompanies();
  }

  loadCompanies() {
    this.lookupCompanyObserver = this.companyService.findAll(0, [], '', '', -1);
    this.lookupCompanyObserver.subscribe(
      data => {
        this.lookupCompanies = data;
      },
      err => {
        console.log(err)
      }
    )
  }

  onCompany() {
    this.salesExpenseService.findAll(0, [
      { lop: "AND", col: "sale_expenses.company_id", cop: "=", val: this.company_id?.value },
      { lop: "AND", col: "sale_expenses.role_type", cop: "=", val: UserTypes.EMPLOYEE },
    ], '', '', -1).subscribe(
      data => {
        this.employee_sales = data
        this.employee_sales_sum = this.onTotalSalesSum(data)
      },
      err => {
        console.log(err)
      }
    )
    this.salesExpenseService.findAll(0, [
      { lop: "AND", col: "sale_expenses.company_id", cop: "=", val: this.company_id?.value },
      { lop: "AND", col: "sale_expenses.role_type", cop: "=", val: UserTypes.DISTRIBUTOR },
    ], '', '', -1).subscribe(
      data => {
        this.distributer_sales = data
        this.distributer_sales_sum = this.onTotalSalesSum(data)
        this.companySelected = true
      },
      err => {
        console.log(err)
      }
    )
  }

  onTotalProduct(product: any): number {
    let total_product: number = 0;
    product.forEach((element: any) => {
      total_product+=element.product_unit
    });
    return total_product;
  }

  onTotalSalesSum(employee_data: any): any {
    let sales_sum: any = []
    employee_data.forEach((element: any) => {
      let product_unit_sum: number = 0;
      let name: string = '';
      let product_sum: any=[]
      element.product_units.forEach((product: any) => {
        product_unit_sum+= +product.product_unit
        name= product.name
        product_sum.push({name: product.name, product_unit: product.product_unit});
      });
      sales_sum.push(product_sum)
    });
    console.log(this.onTotalSum(sales_sum))
    return this.onTotalSum(sales_sum)
  }

  onTotalSum(arr: any): any {
    let arr_sum: any =[]
    arr.forEach((product: any, index: number) => {
      let product_unit_sum: any= []
      product.forEach((key: any, product_index: number) => {
        if(index==0)
        arr_sum.push(key.product_unit)
        else
        arr_sum[product_index]+= +key.product_unit
      });
    });
    return arr_sum;
  }

  get company_id() {
    return this.form.get("company_id");
  }

}
