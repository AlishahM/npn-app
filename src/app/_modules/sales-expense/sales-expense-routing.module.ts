import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SalesExpenseAddUpdateComponent } from './sales-expense-add-update/sales-expense-add-update.component';
import { SalesExpenseDetailComponent } from './sales-expense-detail';
import { SalesExpenseListComponent } from './sales-expense-list/sales-expense-list.component';
import { SalesExpenseComponent } from './sales-expense.component';

const routes: Routes = [{
  path: '', component: SalesExpenseComponent, children: [
    { path: 'sales-expense-list', component: SalesExpenseListComponent },
    { path: 'add-sales-expense', component: SalesExpenseAddUpdateComponent },
    { path: 'sales-expense-detail', component: SalesExpenseDetailComponent },
    { path: '', redirectTo: 'sales-expense-list' }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SalesExpenseRoutingModule { }
