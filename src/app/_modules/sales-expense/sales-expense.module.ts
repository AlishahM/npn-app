import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SalesExpenseRoutingModule } from './sales-expense-routing.module';
import { SalesExpenseComponent } from './sales-expense.component';
import { SalesExpenseAddUpdateComponent } from './sales-expense-add-update/sales-expense-add-update.component';
import { SalesExpenseListComponent } from './sales-expense-list/sales-expense-list.component';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatListModule } from '@angular/material/list';
import { MatTableModule } from '@angular/material/table';
import { ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { SharedModule } from '../shared/shared.module';
import { MatSelectModule } from '@angular/material/select';
import { SalesExpenseDetailComponent } from './sales-expense-detail/sales-expense-detail.component';


@NgModule({
  declarations: [
    SalesExpenseComponent,
    SalesExpenseAddUpdateComponent,
    SalesExpenseListComponent,
    SalesExpenseDetailComponent
  ],
  imports: [
    CommonModule,
    SalesExpenseRoutingModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatPaginatorModule,
    MatSortModule,
    MatListModule,
    MatTableModule,
    ReactiveFormsModule,
    MatInputModule,
    SharedModule,
    MatSelectModule
  ]
})
export class SalesExpenseModule { }
