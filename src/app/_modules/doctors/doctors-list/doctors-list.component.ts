import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { merge } from 'rxjs';
import { tap } from 'rxjs/operators';
import { DoctorService } from 'src/app/_services';
import { DoctorsAddUpdateComponent } from '../doctors-add-update/doctors-add-update.component';
import { DoctorsDatasource } from './doctors-datasource';

@Component({
  selector: 'app-doctors-list',
  templateUrl: './doctors-list.component.html',
  styleUrls: ['./doctors-list.component.scss']
})
export class DoctorsListComponent implements OnInit {

  dataSource: DoctorsDatasource | any;

  displayedColumns = ["id", "firstName", "lastName", "speciality", "type", "status"];

  @ViewChild(MatPaginator) paginator: MatPaginator | any;
  @ViewChild(MatSort)
  sort: MatSort = new MatSort;

  constructor(private dialog: MatDialog,
    private doctorService: DoctorService
  ) { }

  ngOnInit(): void {
    this.dataSource = new DoctorsDatasource(this.doctorService);
  }

  ngAfterViewInit() {
    Promise.resolve().then(() => {

      this.dataSource?.loadDoctors(0, [], '', 'asc', 1, 10);

      this.sort?.sortChange.subscribe(() => this.paginator.pageIndex = 0);

      merge(this.sort?.sortChange, this.paginator?.page)
        .pipe(
          tap(() => this.loadDoctorPage())
        )
        .subscribe();
    });
  }

  onDoctor() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.width = "930px";
    dialogConfig.autoFocus = false;
    dialogConfig.data = {};
    const dialogRef = this.dialog.open(DoctorsAddUpdateComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(
      data => {
        this.loadDoctorPage()
      }
    );
  }

  loadDoctorPage() {
    console.log('loadEmployeePage()');
    console.log(this.paginator?.pageIndex)
    this.dataSource?.loadDoctors(
      0,
      [
        // { "col": "crc", "op" : "like", "val": this.filterCrc?.nativeElement.value},
        // { "col": "name", "op" : "like", "val": this.filterName?.nativeElement.value}
      ],
      this.sort?.active,
      this.sort?.direction,
      this.paginator?.pageIndex + 1,
      this.paginator?.pageSize);
  }
}
