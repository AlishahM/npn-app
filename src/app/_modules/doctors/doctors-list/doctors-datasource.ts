import { CollectionViewer, DataSource } from "@angular/cdk/collections";
import { Observable, BehaviorSubject, of } from "rxjs";
import { catchError, finalize } from "rxjs/operators";

import { DoctorService } from '../../../_services';

export class DoctorsDatasource implements DataSource<any> {

    totalItems = 0;

    private doctorSubject = new BehaviorSubject<any[]>([]);

    private loadingSubject = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingSubject.asObservable();

    constructor(private doctorService: DoctorService) { }

    loadDoctors(courseId: number,
        filters: any[],
        sortBy: string,
        sortDirection: string,
        pageNumber: number,
        pageSize: number) {

        this.loadingSubject.next(true);
        this.doctorService.findAll(courseId, filters, sortBy, sortDirection,
            pageNumber, pageSize).pipe(
                catchError(() => of([])),
                finalize(() => this.loadingSubject.next(false))
            )
            .subscribe(data => {
                this.totalItems = data.total;
                return this.doctorSubject.next(data.data);
            });

    }

    connect(collectionViewer: CollectionViewer): Observable<any[]> {
        // bind observer
        console.log("Connecting data source");
        return this.doctorSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.doctorSubject.complete();
        this.loadingSubject.complete();
    }

}

