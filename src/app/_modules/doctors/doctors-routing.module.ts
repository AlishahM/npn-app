import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DoctorsAddUpdateComponent } from './doctors-add-update/doctors-add-update.component';
import { DoctorsListComponent } from './doctors-list/doctors-list.component';
import { DoctorsComponent } from './doctors.component';

const routes: Routes = [{ 
  path: '', component: DoctorsComponent, children: [
    { path: 'doctors-list', component: DoctorsListComponent },
    { path: 'doctors-add-update', component: DoctorsAddUpdateComponent },
    { path: '', redirectTo: 'doctors-list' }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DoctorsRoutingModule { }
