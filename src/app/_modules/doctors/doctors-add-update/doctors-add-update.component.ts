import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { validateAllFormFields } from 'src/app/_helpers';
import { DoctorService } from 'src/app/_services';

@Component({
  selector: 'app-doctors-add-update',
  templateUrl: './doctors-add-update.component.html',
  styleUrls: ['./doctors-add-update.component.scss']
})
export class DoctorsAddUpdateComponent implements OnInit {

  loading: boolean = false;
  updated: boolean = false;
  errors: any;

  // Initializing Form with fields
  doctorForm = new FormGroup({
    id: new FormControl(null, []),
    firstName: new FormControl('', [Validators.required]),
    lastName: new FormControl('', [Validators.required]),
    address: new FormControl('', [Validators.required]),
    dob: new FormControl('', [Validators.required]),
    dom: new FormControl('', [Validators.required]),
    phoneNumber: new FormControl('', [Validators.required]),
    speciality: new FormControl('', [Validators.required]),
    type: new FormControl('', [Validators.required]),
    status: new FormControl('', [Validators.required]),
  });

  typeList = [
    {value: "A"},
    {value: "A-"},
    {value: "B"},
    {value: "B-"},
    {value: "C"}
  ]

  constructor(
    public dialogRef: MatDialogRef<DoctorsAddUpdateComponent>,
    private doctorService: DoctorService
  ) { }

  ngOnInit(): void {
  }

  onSubmit() {
    if (this.doctorForm.valid) {
      this.doctorService.store(this.doctorForm.value).subscribe(
        data => {
          console.log(data)
          this.onClose()
        },
        err => {
          console.log(err)
          this.errors = err.errors
        }
      )
    } else
      validateAllFormFields(this.doctorForm);
  }

  onClear(): void {
    this.doctorForm.reset()
  }

  onClose() {
    this.dialogRef.close({ updated: this.updated })
  }

  // Getters for form fields
  get firstName() {
    return this.doctorForm.get('firstName')
  }
  get lastName() {
    return this.doctorForm.get('lastName')
  }
  get address() {
    return this.doctorForm.get('address')
  }
  get dob() {
    return this.doctorForm.get('dob')
  }
  get dom() {
    return this.doctorForm.get('dom')
  }
  get phoneNumber() {
    return this.doctorForm.get('phoneNumber')
  }
  get speciality() {
    return this.doctorForm.get('speciality')
  }
  get type() {
    return this.doctorForm.get('type')
  }
  get status() {
    return this.doctorForm.get('status')
  }
}
