import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DoctorsAddUpdateComponent } from './doctors-add-update.component';

describe('DoctorsAddUpdateComponent', () => {
  let component: DoctorsAddUpdateComponent;
  let fixture: ComponentFixture<DoctorsAddUpdateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DoctorsAddUpdateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DoctorsAddUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
