import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PromotionalMaterialAddUpdateComponent } from './promotional-material-add-update.component';

describe('PromotionalMaterialAddUpdateComponent', () => {
  let component: PromotionalMaterialAddUpdateComponent;
  let fixture: ComponentFixture<PromotionalMaterialAddUpdateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PromotionalMaterialAddUpdateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PromotionalMaterialAddUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
