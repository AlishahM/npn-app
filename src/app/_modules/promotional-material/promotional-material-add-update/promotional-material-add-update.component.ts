import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { validateAllFormFields } from 'src/app/_helpers';
import { PromotionalMaterialService } from 'src/app/_services';

@Component({
  selector: 'app-promotional-material-add-update',
  templateUrl: './promotional-material-add-update.component.html',
  styleUrls: ['./promotional-material-add-update.component.scss']
})
export class PromotionalMaterialAddUpdateComponent implements OnInit {

  id: any;
  loading: boolean = false;
  updated: boolean = false;
  errors: any;

  // Initializing Form with fields
  promotionalMaterialForm = new FormGroup({
    id: new FormControl(null, []),
    name: new FormControl('', [Validators.required]),
    price: new FormControl('', [Validators.required]),
    quantity: new FormControl('', [Validators.required]),
  });

  constructor(
    public dialogRef: MatDialogRef<PromotionalMaterialAddUpdateComponent>,
    private promotionalMaterialService: PromotionalMaterialService,
    @Inject(MAT_DIALOG_DATA) data: any
  ) {
    if (data) this.promotionalMaterialForm.get("id")?.setValue(data.id)
  }

  ngOnInit(): void {
    if (this.promotionalMaterialForm.get("id")?.value) {
      this.loading = true;
      // this.productService.find(this.promotionalMaterialForm.get("id")?.value).subscribe(
      //   data => {
      //     this.fillForm(data)
      //     this.loading = false
      //   }
      // )
    }
  }

  fillForm(product: any) {
    Object.keys(this.promotionalMaterialForm.controls).forEach((key, index) => {
      this.promotionalMaterialForm.controls[key].markAsDirty();
      this.promotionalMaterialForm.controls[key].patchValue(product[key], {
        onlySelf: true,
      });
    });
  }

  onSubmit(): void {
    if (this.promotionalMaterialForm.valid) {
      if (!this.promotionalMaterialForm.get("id")?.value) {
          this.promotionalMaterialService.store(this.promotionalMaterialForm.value).subscribe(
            data => {
              console.log(data)
              this.onClose()
            },
            err => {
              console.log(err)
              this.errors = err.errors
            }
          )
      } else {
          this.promotionalMaterialService.update(this.promotionalMaterialForm.value).subscribe(
            data => {
              console.log(data)
              this.updated = true
              this.onClose()
            },
            err => {
              console.log(err)
              this.errors = err.errors
            }
          )
      }
    } else
      validateAllFormFields(this.promotionalMaterialForm);
  }

  onClear(): void {
    this.promotionalMaterialForm.reset()
  }

  onClose() {
    this.dialogRef.close({ updated: this.updated })
  }

  // Getters for form fields
  get name() {
    return this.promotionalMaterialForm.get('name')
  }
  get price() {
    return this.promotionalMaterialForm.get('price')
  }
  get quantity() {
    return this.promotionalMaterialForm.get('quantity')
  }

}
