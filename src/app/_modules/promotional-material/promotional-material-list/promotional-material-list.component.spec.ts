import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PromotionalMaterialListComponent } from './promotional-material-list.component';

describe('PromotionalMaterialListComponent', () => {
  let component: PromotionalMaterialListComponent;
  let fixture: ComponentFixture<PromotionalMaterialListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PromotionalMaterialListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PromotionalMaterialListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
