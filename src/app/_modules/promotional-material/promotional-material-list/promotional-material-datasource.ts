import { CollectionViewer, DataSource } from "@angular/cdk/collections";
import { Observable, BehaviorSubject, of } from "rxjs";
import { catchError, finalize } from "rxjs/operators";

import { PromotionalMaterialService } from '../../../_services';

export class PromotionalMaterialDatasource implements DataSource<any> {

    totalItems = 0;

    private promotionalMaterialSubject = new BehaviorSubject<any[]>([]);

    private loadingSubject = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingSubject.asObservable();

    constructor(private promotionalMaterialService: PromotionalMaterialService) { }

    loadPromotionalMaterial(courseId: number,
        filters: any[],
        sortBy: string,
        sortDirection: string,
        pageNumber: number,
        pageSize: number) {

        this.loadingSubject.next(true);
        this.promotionalMaterialService.findAll(courseId, filters, sortBy, sortDirection,
            pageNumber, pageSize).pipe(
                catchError(() => of([])),
                finalize(() => this.loadingSubject.next(false))
            )
            .subscribe(data => {
                this.totalItems = data.total;
                return this.promotionalMaterialSubject.next(data.data);
            });

    }

    connect(collectionViewer: CollectionViewer): Observable<any[]> {
        // bind observer
        console.log("Connecting data source");
        return this.promotionalMaterialSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.promotionalMaterialSubject.complete();
        this.loadingSubject.complete();
    }

}

