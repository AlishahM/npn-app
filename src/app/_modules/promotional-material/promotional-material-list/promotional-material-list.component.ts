import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { merge } from 'rxjs';
import { tap } from 'rxjs/operators';
import { PromotionalMaterialService } from 'src/app/_services';
import { PromotionalMaterialAddUpdateComponent } from '../promotional-material-add-update/promotional-material-add-update.component';
import { PromotionalMaterialDatasource } from './promotional-material-datasource';

@Component({
  selector: 'app-promotional-material-list',
  templateUrl: './promotional-material-list.component.html',
  styleUrls: ['./promotional-material-list.component.scss']
})
export class PromotionalMaterialListComponent implements OnInit {

  dataSource: PromotionalMaterialDatasource | any;

  displayedColumns = ["id", "name", "price", "quantity"];

  @ViewChild(MatPaginator) paginator: MatPaginator | any;
  @ViewChild(MatSort)
  sort: MatSort = new MatSort;

  @ViewChild('reload') reload: ElementRef | undefined;

  constructor(
    private promotionalMaterialService: PromotionalMaterialService,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.dataSource = new PromotionalMaterialDatasource(this.promotionalMaterialService);
  }

  ngAfterViewInit() {
    Promise.resolve().then(() => {

      this.dataSource?.loadPromotionalMaterial(0, [], '', 'asc', 1, 10);

      this.sort?.sortChange.subscribe(() => this.paginator.pageIndex = 0);

      merge(this.sort.sortChange, this.paginator.page)
        .pipe(
          tap(() => this.loadPromotionalMaterialPage())
        )
        .subscribe();
    });
  }

  loadPromotionalMaterialPage() {
    console.log('loadPromotionalMaterialPage()');
    console.log(this.paginator.pageIndex)
    this.dataSource?.loadPromotionalMaterial(
      0,
      [
        // { "col": "crc", "op" : "like", "val": this.filterCrc?.nativeElement.value},
        // { "col": "name", "op" : "like", "val": this.filterName?.nativeElement.value}
      ],
      this.sort.active,
      this.sort.direction,
      this.paginator.pageIndex + 1,
      this.paginator.pageSize);
  }

  onPromotionalMaterial() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.width = "930px";
    dialogConfig.autoFocus = false;
    dialogConfig.data = {};
    const dialogRef = this.dialog.open(PromotionalMaterialAddUpdateComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(
      data => {
        this.loadPromotionalMaterialPage()
      }
    );
  }

  onEdit(row: any) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.width = "930px";
    dialogConfig.autoFocus = false;
    dialogConfig.data = {
      id: row.id
    };
    const dialogRef = this.dialog.open(PromotionalMaterialAddUpdateComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(
      data => {
        if (data) this.loadPromotionalMaterialPage()
      }
    );
  }

}
