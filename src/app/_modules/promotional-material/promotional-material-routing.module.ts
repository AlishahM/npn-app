import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PromotionalMaterialAddUpdateComponent } from './promotional-material-add-update/promotional-material-add-update.component';
import { PromotionalMaterialListComponent } from './promotional-material-list/promotional-material-list.component';
import { PromotionalMaterialComponent } from './promotional-material.component';

const routes: Routes = [{
  path: '', component: PromotionalMaterialComponent, children: [
    { path: 'promotional-material-list', component: PromotionalMaterialListComponent },
    { path: 'add-promotional-material', component: PromotionalMaterialAddUpdateComponent },
    { path: '', redirectTo: 'promotional-material-list' }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PromotionalMaterialRoutingModule { }
