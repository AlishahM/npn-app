import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PromotionalMaterialRoutingModule } from './promotional-material-routing.module';
import { PromotionalMaterialComponent } from './promotional-material.component';
import { PromotionalMaterialListComponent } from './promotional-material-list/promotional-material-list.component';
import { PromotionalMaterialAddUpdateComponent } from './promotional-material-add-update/promotional-material-add-update.component';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatListModule } from '@angular/material/list';
import { MatTableModule } from '@angular/material/table';
import { ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { SharedModule } from '../shared/shared.module';
import { MatSelectModule } from '@angular/material/select';


@NgModule({
  declarations: [
    PromotionalMaterialComponent,
    PromotionalMaterialListComponent,
    PromotionalMaterialAddUpdateComponent
  ],
  imports: [
    CommonModule,
    PromotionalMaterialRoutingModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatPaginatorModule,
    MatSortModule,
    MatListModule,
    MatTableModule,
    ReactiveFormsModule,
    MatInputModule,
    SharedModule,
    MatSelectModule
  ]
})
export class PromotionalMaterialModule { }
