import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PromotionalMaterialComponent } from './promotional-material.component';

describe('PromotionalMaterialComponent', () => {
  let component: PromotionalMaterialComponent;
  let fixture: ComponentFixture<PromotionalMaterialComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PromotionalMaterialComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PromotionalMaterialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
