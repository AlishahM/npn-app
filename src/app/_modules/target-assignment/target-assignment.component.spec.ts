import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TargetAssignmentComponent } from './target-assignment.component';

describe('TargetAssignmentComponent', () => {
  let component: TargetAssignmentComponent;
  let fixture: ComponentFixture<TargetAssignmentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TargetAssignmentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TargetAssignmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
