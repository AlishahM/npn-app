import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TargetAssignmentListComponent } from './target-assignment-list/target-assignment-list.component';
import { TargetAssignmentComponent } from './target-assignment.component';

const routes: Routes = [{ path: '', component: TargetAssignmentComponent, children: [
  { path: 'target-assignment-list', component: TargetAssignmentListComponent },
  { path: '', redirectTo: 'target-assignment-list' }
] }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TargetAssignmentRoutingModule { }
