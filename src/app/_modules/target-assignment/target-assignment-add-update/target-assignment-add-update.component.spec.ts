import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TargetAssignmentAddUpdateComponent } from './target-assignment-add-update.component';

describe('TargetAssignmentAddUpdateComponent', () => {
  let component: TargetAssignmentAddUpdateComponent;
  let fixture: ComponentFixture<TargetAssignmentAddUpdateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TargetAssignmentAddUpdateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TargetAssignmentAddUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
