import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { validateAllFormFields } from 'src/app/_helpers';
import { Company, Products, UserTypes, Zone } from 'src/app/_models';
import { AuthenticationService, CompanyService, ProductsService, ZoneService } from 'src/app/_services';
import { TargetAssignmentService } from 'src/app/_services/target-assignment/target-assignment.service';
import { UserService } from 'src/app/_services/user/user.service';

@Component({
  selector: 'app-target-assignment-add-update',
  templateUrl: './target-assignment-add-update.component.html',
  styleUrls: ['./target-assignment-add-update.component.scss']
})
export class TargetAssignmentAddUpdateComponent implements OnInit {

  targetAssignmentForm = new FormGroup({
    id: new FormControl(null, []),
    user_id: new FormControl(null, []),
    role_type: new FormControl(null, []),
    company_id: new FormControl('', [Validators.required]),
    zone_id: new FormControl('', [Validators.required]),
    year: new FormControl('', [Validators.required]),
    month: new FormControl('', [Validators.required]),
    target_assignment_details: new FormArray([]),
  });

  lookupCompanyObserver: Observable<any[]> | null | undefined;
  lookupCompanies: Company[] = [];
  lookupZoneObserver: Observable<any[]> | null | undefined;
  lookupZones: Zone[] = [];
  lookupProsuctObserver: Observable<any[]> | null | undefined;
  lookupProducts: Products[] = [];

  lookupUsers:any[] = [];

  loading: boolean = false;
  updated: boolean = false;
  updating: boolean = false;
  isEditMode: boolean = false;

  errors: any[] = [];

  constructor(
    private dialogRef: MatDialogRef<TargetAssignmentAddUpdateComponent>,
    private companyService: CompanyService,
    private zoneService: ZoneService,
    private userService: UserService,
    private productService: ProductsService,
    private authenticationService: AuthenticationService,
    private targetAssignmentService: TargetAssignmentService
  ) { }

  ngOnInit(): void {
    this.loadCompanies();
    this.loadZones();
    this.loadProducts();
    if (this.isEditMode) {
      // this.getPurchaseOrderById();
      this.authenticationService.currentEmployee.subscribe(
        (data: any) => {
          console.log(data.user)
          // this.employee = data.user
        });
    } else {
      this.authenticationService.currentEmployee.subscribe(
        (data: any) => {
          console.log(data.user)
          this.user_id?.setValue(data.user.id)
          this.role_type?.setValue(data.user.role_type)
        }
      )
    }
  }

  loadCompanies() {
    this.lookupCompanyObserver = this.companyService.findAll(0, [], '', '', -1);
    this.lookupCompanyObserver.subscribe(
      data => {
        this.lookupCompanies = data;
      },
      err => {
        console.log(err)
      }
    )
  }

  loadZones() {
    this.lookupZoneObserver = this.zoneService.findAll(0, [], '', '', -1);
    this.lookupZoneObserver.subscribe(
      data => {
        this.lookupZones = data;
      },
      err => {
        console.log(err)
      }
    )
  }

  loadProducts() {
    if (this.company_id?.value) {
      this.productService.findAll(0, [
        { lop: "AND", col:"company_id", cop:"=", val:this.company_id.value }
      ], '', '', -1).subscribe(
        data => {
          this.lookupProducts = data;
        },
        err => {
          this.errors = err.errors;
        }
      )
    }
  }

  loadUsers() {
    if (this.company_id?.value && this.zone_id?.value) {
      this.userService.findAll(
        [
          { lop: "AND", col:"users.company_id", cop:"=", val:this.company_id.value },
          { lop: "AND", col:"users.zone_id", cop:"=", val:this.zone_id.value },
          { lop: "AND", col:"users.role_type", cop:"=", val:UserTypes.EMPLOYEE }
        ],
        '',
        '',
        -1
      ).subscribe(
        data => {
          this.lookupUsers = data;
          this.target_assignment_details.clear();
          data.forEach((employee: any) => {
            this.lookupProducts.forEach((products: any) => {
              this.onAddSaleAssignment(
                {
                  id: products.id,
                  name: products.name,
                  employee_id: employee.id,
                  employee_name: employee.name,
                  order_unit: 0
                }
                )
            });
          });
        },
        err => {
          this.errors = err.errors;
        }
      )
    }
  }

  onAddSaleAssignment(productUnit: any) {
      const itemGrp = new FormGroup({
        id: new FormControl("", []),
        product_id: new FormControl(productUnit.id, [Validators.required]),
        product_name: new FormControl(productUnit.name, [Validators.required]),
        employee_id: new FormControl(productUnit.employee_id, [Validators.required]),
        employee_name: new FormControl(productUnit.employee_name, [Validators.required]),
        order_unit: new FormControl(productUnit?.order_unit, [Validators.required])
      });
      // itemGrp.patchValue(element, { onlySelf: true });
      this.target_assignment_details.push(itemGrp);
    
  }

  // onAddSaleAssignmentByPO(productUnit: any) {
  //   productUnit.forEach((element: any) => {
  //     const itemGrp = new FormGroup({
  //       id: new FormControl(element.id, []),
  //       product_id: new FormControl(element.product_id, [Validators.required]),
  //       product_name: new FormControl(element.product_name, [Validators.required]),
  //       price: new FormControl(element.price, [Validators.required]),
  //       order_unit: new FormControl(element.order_unit, [Validators.required])
  //     });
  //     // itemGrp.patchValue(element, { onlySelf: true });
  //     this.product_units.push(itemGrp);
  //   });
  // }

  onSubmit() {
    if (this.targetAssignmentForm.valid) {
      if (!this.isEditMode) {
        this.targetAssignmentService.store(this.targetAssignmentForm.value).subscribe(
          data => {
            console.log(data)
            this.updated = true;
            this.onClose()
          },
          err => {
            this.errors = err.errors;
          }
        )
      }
    } else {
      validateAllFormFields(this.targetAssignmentForm)
    }
  }

  onClose() {
    this.dialogRef.close({updated: this.updated})
  }

  get user_id() {
    return this.targetAssignmentForm.get("user_id");
  }
  get role_type() {
    return this.targetAssignmentForm.get("role_type");
  }
  get company_id() {
    return this.targetAssignmentForm.get("company_id");
  }
  get zone_id() {
    return this.targetAssignmentForm.get("zone_id");
  }
  get year() {
    return this.targetAssignmentForm.get("year");
  }
  get month() {
    return this.targetAssignmentForm.get("month");
  }
  get target_assignment_details() {
    return this.targetAssignmentForm.controls["target_assignment_details"] as FormArray;
  }

}
