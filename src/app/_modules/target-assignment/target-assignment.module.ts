import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TargetAssignmentRoutingModule } from './target-assignment-routing.module';
import { TargetAssignmentComponent } from './target-assignment.component';
import { TargetAssignmentListComponent } from './target-assignment-list/target-assignment-list.component';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatListModule } from '@angular/material/list';
import { MatTableModule } from '@angular/material/table';
import { ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { SharedModule } from '../shared/shared.module';
import { MatSelectModule } from '@angular/material/select';
import { TargetAssignmentAddUpdateComponent } from './target-assignment-add-update/target-assignment-add-update.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';


@NgModule({
  declarations: [
    TargetAssignmentComponent,
    TargetAssignmentListComponent,
    TargetAssignmentAddUpdateComponent
  ],
  imports: [
    CommonModule,
    TargetAssignmentRoutingModule,
    MatPaginatorModule,
    MatSortModule,
    MatListModule,
    MatTableModule,
    ReactiveFormsModule,
    MatInputModule,
    SharedModule,
    MatSelectModule,
    MatProgressSpinnerModule
  ]
})
export class TargetAssignmentModule { }
