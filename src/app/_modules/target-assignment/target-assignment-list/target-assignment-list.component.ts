import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Observable } from 'rxjs';
import { Company } from 'src/app/_models';
import { CompanyService, ZoneService } from 'src/app/_services';
import { TargetAssignmentService } from 'src/app/_services/target-assignment/target-assignment.service';
import { TargetAssignmentAddUpdateComponent } from '../target-assignment-add-update/target-assignment-add-update.component';
import { TargetAssignmentDatasource } from './target-assignment-datasource';

@Component({
  selector: 'app-target-assignment-list',
  templateUrl: './target-assignment-list.component.html',
  styleUrls: ['./target-assignment-list.component.scss']
})
export class TargetAssignmentListComponent implements OnInit {

  dataSource: TargetAssignmentDatasource | any;

  displayedColumns: string[] =['employee_name', 'year', 'month', 'product_name', 'order_unit']

  @ViewChild(MatPaginator) paginator: MatPaginator | any;
  @ViewChild(MatSort)
  sort: MatSort = new MatSort;

  @ViewChild('reload') reload: ElementRef | undefined;

  form = new FormGroup({
    company_id: new FormControl('', [Validators.required]),
    zone_id: new FormControl('', [Validators.required]),
    year: new FormControl('', [Validators.required]),
    month: new FormControl('', [Validators.required]),
  });

  lookupCompanyObserver: Observable<any[]> | null | undefined;
  lookupCompanies: Company[] = [];
  lookupZoneObserver: Observable<any[]> | null | undefined;
  lookupZones: any[] = [];

  constructor(
    private dialog: MatDialog,
    private companyService: CompanyService,
    private zoneService: ZoneService,
    private targetAssignmentService: TargetAssignmentService
    ) { }

  ngOnInit(): void {
    this.dataSource = new TargetAssignmentDatasource(this.targetAssignmentService);
    this.loadCompanies();
    this.loadZones();
  }

  loadTargetAssignmentPage() {
    if (this.company_id?.value && this.zone_id?.value && this.year?.value && this.month?.value) {
      this.dataSource?.loadTargetAssignment(
        this.company_id.value,
        this.zone_id.value,
        this.year.value,
        this.month.value
      );
      }
  }

  loadCompanies() {
    this.lookupCompanyObserver = this.companyService.findAll(0, [], '', '', -1);
    this.lookupCompanyObserver.subscribe(
      data => {
        console.log(data)
        this.lookupCompanies = data;
      },
      err => {
        console.log(err)
      }
    )
  }

  loadZones() {
    this.lookupZoneObserver = this.zoneService.findAll(0, [], '', '', -1);
    this.lookupZoneObserver.subscribe(
      data => {
        console.log(data)
        this.lookupZones = data;
      },
      err => {
        console.log(err)
      }
    )
  }

  onAddTarget() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.minWidth = "75vw";
    dialogConfig.minHeight = "95vh";
    dialogConfig.autoFocus = false;
    dialogConfig.disableClose = true;
    dialogConfig.data = {};
    const dialogRef = this.dialog.open(TargetAssignmentAddUpdateComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(
      data => {
        // if (data.updated)
        //
      }
    );
  }

  get company_id() {
    return this.form.get("company_id");
  }
  get zone_id() {
    return this.form.get("zone_id");
  }
  get year() {
    return this.form.get("year");
  }
  get month() {
    return this.form.get("month");
  }

}
