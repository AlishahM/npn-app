import { CollectionViewer, DataSource } from "@angular/cdk/collections";
import { Observable, BehaviorSubject, of } from "rxjs";
import { catchError, finalize } from "rxjs/operators";
import { TargetAssignmentService } from "src/app/_services/target-assignment/target-assignment.service";

export class TargetAssignmentDatasource implements DataSource<any> {

    totalItems = 0;

    private targetAssignmentSubject = new BehaviorSubject<any[]>([]);

    private loadingSubject = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingSubject.asObservable();

    constructor(private targetAssignmentService: TargetAssignmentService) { }

    loadTargetAssignment(companyId: number,
        zoneId: number,
        year: number,
        month: number) {

        this.loadingSubject.next(true);
        this.targetAssignmentService.getTargetAssignmentDetail(companyId, zoneId, year, month).pipe(
                catchError(() => of([])),
                finalize(() => this.loadingSubject.next(false))
            )
            .subscribe((data:any) => {
                return this.targetAssignmentSubject.next(data);
            });

    }

    connect(collectionViewer: CollectionViewer): Observable<any[]> {
        // bind observer
        console.log("Connecting data source");
        return this.targetAssignmentSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.targetAssignmentSubject.complete();
        this.loadingSubject.complete();
    }

}

