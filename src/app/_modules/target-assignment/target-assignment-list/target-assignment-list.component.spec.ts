import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TargetAssignmentListComponent } from './target-assignment-list.component';

describe('TargetAssignmentListComponent', () => {
  let component: TargetAssignmentListComponent;
  let fixture: ComponentFixture<TargetAssignmentListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TargetAssignmentListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TargetAssignmentListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
