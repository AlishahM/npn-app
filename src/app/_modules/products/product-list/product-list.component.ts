import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ProductAddUpdateComponent } from '../product-add-update';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ProductsDatasource } from './product-datasource';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ActivatedRoute } from '@angular/router';
import { ProductsService } from 'src/app/_services';
import { merge } from 'rxjs';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {

  // loading = false;

  dataSource: ProductsDatasource | any;

  displayedColumns = ["id", "name", "price", "actions"];

  @ViewChild(MatPaginator) paginator: MatPaginator | any;
  @ViewChild(MatSort)
  sort: MatSort = new MatSort;

  // @ViewChild('filterId') filterId: ElementRef | undefined;
  // @ViewChild('filterName') filterName: ElementRef | undefined;
  // @ViewChild('filterCrc') filterCrc: ElementRef | undefined;
  // @ViewChild('filterDescription') filterDescription: ElementRef | undefined;
  // @ViewChild('filterOthers') filterOthers: ElementRef | undefined;
  @ViewChild('reload') reload: ElementRef | undefined;

  constructor(
      private route: ActivatedRoute,
      private productsService: ProductsService,
      private dialog: MatDialog,
      //private snackBService:SnackBarService
  ) {
  }

  ngOnInit() {
      this.dataSource = new ProductsDatasource(this.productsService);
  }

  ngAfterViewInit() {
      Promise.resolve().then(() => {

          this.dataSource?.loadProducts(0, [], '', 'asc', 1, 10);

          this.sort?.sortChange.subscribe(() => this.paginator.pageIndex = 0);



          // fromEvent(this.filterName?.nativeElement,'change')
          //     .pipe(
          //         debounceTime(150), 
          //         distinctUntilChanged(),
          //         tap(() => {
          //             this.paginator.pageIndex = 0;
          //             this.loadProductsPage();
          //         })
          //     )
          //     .subscribe();

          // fromEvent(this.filterCrc?.nativeElement,'change')
          //     .pipe(
          //         debounceTime(150), 
          //         distinctUntilChanged(),
          //         tap(() => {
          //             this.paginator.pageIndex = 0;
          //             this.loadProductsPage();
          //         })
          //     )
          //     .subscribe(); 


          // fromEvent(this.filterDescription?.nativeElement,'change')
          //     .pipe(
          //         debounceTime(150), 
          //         distinctUntilChanged(),
          //         tap(() => {
          //             this.paginator.pageIndex = 0;
          //             this.loadProductsPage();
          //         })
          //     )
          //     .subscribe();


          // fromEvent(this.filterOthers?.nativeElement,'change')
          //     .pipe(
          //         debounceTime(150), 
          //         distinctUntilChanged(),
          //         tap(() => {
          //             this.paginator.pageIndex = 0;
          //             this.loadProductsPage();
          //         })
          //     )
          //     .subscribe();

          merge(this.sort.sortChange, this.paginator.page)
              .pipe(
                  tap(() => this.loadProductsPage())
              )
              .subscribe();
      });
  }

  loadProductsPage() {
      console.log('loadProductsPage()');
      console.log(this.paginator.pageIndex)
      this.dataSource?.loadProducts(
          0,
          [
              // { "col": "crc", "op" : "like", "val": this.filterCrc?.nativeElement.value},
              // { "col": "name", "op" : "like", "val": this.filterName?.nativeElement.value}
          ],
          this.sort.active,
          this.sort.direction,
          this.paginator.pageIndex + 1,
          this.paginator.pageSize);
  }

  onProduct() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.width = "930px";
    dialogConfig.autoFocus = false;
    dialogConfig.data = {};
    const dialogRef = this.dialog.open(ProductAddUpdateComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(
        data => {
          this.loadProductsPage()
        }
    );
  }

  onEdit(row: any) {
    console.log(row)
    const dialogConfig = new MatDialogConfig();
    dialogConfig.width = "930px";
    dialogConfig.autoFocus = false;
    dialogConfig.data = {
      id: row.id
    };
    const dialogRef = this.dialog.open(ProductAddUpdateComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(
        data => {
          if(data) this.loadProductsPage()
        }
    );
  }

}
