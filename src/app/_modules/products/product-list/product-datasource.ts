import {CollectionViewer, DataSource} from "@angular/cdk/collections";
import {Observable, BehaviorSubject, of} from "rxjs";
import {catchError, finalize} from "rxjs/operators";
import { Products } from "src/app/_models";

import { ProductsService } from '../../../_services';



export class ProductsDatasource implements DataSource<Products> {

    totalItems = 0;
    
    private ProductssSubject = new BehaviorSubject<Products[]>([]);

    private loadingSubject = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingSubject.asObservable();

    constructor(private ProductsService: ProductsService) {

    }

    loadProducts(courseId:number,
                filters:any[],
                sortBy:string,
                sortDirection:string,
                pageNumber:number,
                pageSize:number) {

        this.loadingSubject.next(true);
        this.ProductsService.findAll(courseId, filters, sortBy, sortDirection,
            pageNumber, pageSize).pipe(
                catchError(() => of([])),
                finalize(() => this.loadingSubject.next(false))
            )
            .subscribe(data => {
                this.totalItems = data.total;
                return this.ProductssSubject.next(data.data);
			});

    }

    connect(collectionViewer: CollectionViewer): Observable<Products[]> {
		// bind observer
        console.log("Connecting data source");
        return this.ProductssSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.ProductssSubject.complete();
        this.loadingSubject.complete();
    }

}

