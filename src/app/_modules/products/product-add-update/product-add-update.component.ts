import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { validateAllFormFields } from 'src/app/_helpers';
import { Company } from 'src/app/_models';
import { CompanyService, ProductsService } from 'src/app/_services';

@Component({
  selector: 'app-product-add-update',
  templateUrl: './product-add-update.component.html',
  styleUrls: ['./product-add-update.component.scss']
})
export class ProductAddUpdateComponent implements OnInit {

  lookupCompanyObserver: Observable<any[]> | null | undefined;
  lookupCompanies: Company[] = [];

  id:any;
  loading: boolean = false;
  updated: boolean = false;
  errors: any;

  // Initializing Form with fields
  productForm = new FormGroup({
    id: new FormControl(null, []),
    company_id: new FormControl('', [Validators.required]),
    name: new FormControl('', [Validators.required]),
    price: new FormControl('', [Validators.required]),
  });

  constructor(
    public dialogRef: MatDialogRef<ProductAddUpdateComponent>,
    private productService: ProductsService,
    private companyService: CompanyService,
    @Inject(MAT_DIALOG_DATA) data: any
  ) {
    if(data) this.productForm.get("id")?.setValue(data.id)
  }

  ngOnInit(): void {
    this.loadCompanies();

    if (this.productForm.get("id")?.value) {
      this.loading = true;
      this.productService.find(this.productForm.get("id")?.value).subscribe(
        data => {
          this.fillForm(data)
          this.loading = false
        }
      )
    }
  }

  loadCompanies() {
    this.lookupCompanyObserver = this.companyService.findAll(0, [], '', '', -1);
    this.lookupCompanyObserver.subscribe(
      data => {
        console.log(data)
        this.lookupCompanies = data;
      },
      err => {
        console.log(err)
      }
    )
  }

  fillForm(product: any) {
    Object.keys(this.productForm.controls).forEach((key, index) => {
      this.productForm.controls[key].markAsDirty();
      this.productForm.controls[key].patchValue(product[key], {
        onlySelf: true,
      });
    });
  }

  onSubmit(): void {
    if (this.productForm.valid) {
      if (!this.productForm.get("id")?.value) {
        this.productService.store(this.productForm.value).subscribe(
          data => {
            console.log(data)
            this.onClose()
          },
          err => {
            console.log(err)
            this.errors = err.errors
          }
        )
      } else {
        this.productService.update(this.productForm.value).subscribe(
          data => {
            console.log(data)
            this.updated = true
            this.onClose()
          },
          err => {
            console.log(err)
            this.errors = err.errors
          }
        )
      }
    } else
      validateAllFormFields(this.productForm);
  }

  onClear(): void {
    this.productForm.reset()
  }

  onClose() {
    this.dialogRef.close({updated: this.updated})
  }

  // Getters for form fields
  get company_id() {
    return this.productForm.get('company_id')
  }
  get name() {
    return this.productForm.get('name')
  }
  get price() {
    return this.productForm.get('price')
  }

}
