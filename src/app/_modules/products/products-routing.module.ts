import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductAddUpdateComponent } from './product-add-update';
import { ProductListComponent } from './product-list';
import { ProductsComponent } from './products.component';

const routes: Routes = [
  {
    path: '', component: ProductsComponent, children: [
      { path: 'product-list', component: ProductListComponent },
      { path: 'add-product', component: ProductAddUpdateComponent },
      { path: '', redirectTo: 'product-list' }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductsRoutingModule { }
