import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { validateAllFormFields } from 'src/app/_helpers';
import { Zone } from 'src/app/_models';
import { ZoneService } from 'src/app/_services/zone/zone.service';

@Component({
  selector: 'app-zone-add-update',
  templateUrl: './zone-add-update.component.html',
  styleUrls: ['./zone-add-update.component.scss']
})
export class ZoneAddUpdateComponent implements OnInit {

  id:any;
  loading: boolean = false;
  updated: boolean = false;
  errors: any;

  // Initializing Form with fields
  zoneForm = new FormGroup({
    id: new FormControl(null, []),
    zone: new FormControl('', [Validators.required]),
    area_name: new FormControl('', [Validators.required]),
  });

  constructor(
    public dialogRef: MatDialogRef<ZoneAddUpdateComponent>,
    private zoneService: ZoneService,
    @Inject(MAT_DIALOG_DATA) data: any
  ) {
    if(data) this.zoneForm.get("id")?.setValue(data.id)
  }

  ngOnInit(): void {
    this.loading = true;

    if (this.zoneForm.get("id")?.value) {
      this.zoneService.find(this.zoneForm.get("id")?.value).subscribe(
        data => {
          this.fillForm(data)
          this.loading = false
        }
      )
    }
    this.loading = false;
    return;
  }

  fillForm(product: any) {
    Object.keys(this.zoneForm.controls).forEach((key, index) => {
      this.zoneForm.controls[key].markAsDirty();
      this.zoneForm.controls[key].patchValue(product[key], {
        onlySelf: true,
      });
    });
  }

  onSubmit(): void {
    if (this.zoneForm.valid) {
      if (!this.zoneForm.get("id")?.value) {
        this.zoneService.store(new Zone().deserialize(this.zoneForm.value).compose()).subscribe(
          data => {
            console.log(data)
            this.onClose()
          },
          err => {
            console.log(err)
            this.errors = err.errors
          }
        )
      } else {
        this.zoneService.update(new Zone().deserialize(this.zoneForm.value).compose()).subscribe(
          data => {
            console.log(data)
            this.updated = true
            this.onClose()
          },
          err => {
            console.log(err)
            this.errors = err.errors
          }
        )
      }
    } else
      validateAllFormFields(this.zoneForm);
  }

  onClear(): void {
    this.zoneForm.reset()
  }

  onClose() {
    this.dialogRef.close({updated: this.updated})
  }

  // Getters for form fields
  get zone() {
    return this.zoneForm.get('zone')
  }
  get area_name() {
    return this.zoneForm.get('area_name')
  }

}
