import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ZoneAddUpdateComponent } from './zone-add-update.component';

describe('ZoneAddUpdateComponent', () => {
  let component: ZoneAddUpdateComponent;
  let fixture: ComponentFixture<ZoneAddUpdateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ZoneAddUpdateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ZoneAddUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
