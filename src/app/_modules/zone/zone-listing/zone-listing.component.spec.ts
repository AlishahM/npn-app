import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ZoneListingComponent } from './zone-listing.component';

describe('ZoneListingComponent', () => {
  let component: ZoneListingComponent;
  let fixture: ComponentFixture<ZoneListingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ZoneListingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ZoneListingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
