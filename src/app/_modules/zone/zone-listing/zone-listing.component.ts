import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ZoneAddUpdateComponent } from '../zone-add-update';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ZonesDatasource } from './zone-datasource';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ActivatedRoute } from '@angular/router';
import { ZoneService } from 'src/app/_services';
import { merge } from 'rxjs';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-zone-listing',
  templateUrl: './zone-listing.component.html',
  styleUrls: ['./zone-listing.component.scss']
})
export class ZoneListingComponent implements OnInit {

  // loading = false;

  dataSource: ZonesDatasource | any;

  displayedColumns = ["id", "zone", "area_name", "actions"];

  @ViewChild(MatPaginator) paginator: MatPaginator | any;
  @ViewChild(MatSort)
  sort: MatSort = new MatSort;

  // @ViewChild('filterId') filterId: ElementRef | undefined;
  // @ViewChild('filterName') filterName: ElementRef | undefined;
  // @ViewChild('filterCrc') filterCrc: ElementRef | undefined;
  // @ViewChild('filterDescription') filterDescription: ElementRef | undefined;
  // @ViewChild('filterOthers') filterOthers: ElementRef | undefined;
  @ViewChild('reload') reload: ElementRef | undefined;

  constructor(
      private route: ActivatedRoute,
      private zoneService: ZoneService,
      private dialog: MatDialog,
      //private snackBService:SnackBarService
  ) {
  }

  ngOnInit() {
      this.dataSource = new ZonesDatasource(this.zoneService);
  }

  ngAfterViewInit() {
      Promise.resolve().then(() => {

          this.dataSource?.loadZones(0, [], '', 'asc', 1, 10);

          this.sort?.sortChange.subscribe(() => this.paginator.pageIndex = 0);



          // fromEvent(this.filterName?.nativeElement,'change')
          //     .pipe(
          //         debounceTime(150), 
          //         distinctUntilChanged(),
          //         tap(() => {
          //             this.paginator.pageIndex = 0;
          //             this.loadZonesPage();
          //         })
          //     )
          //     .subscribe();

          // fromEvent(this.filterCrc?.nativeElement,'change')
          //     .pipe(
          //         debounceTime(150), 
          //         distinctUntilChanged(),
          //         tap(() => {
          //             this.paginator.pageIndex = 0;
          //             this.loadZonesPage();
          //         })
          //     )
          //     .subscribe(); 


          // fromEvent(this.filterDescription?.nativeElement,'change')
          //     .pipe(
          //         debounceTime(150), 
          //         distinctUntilChanged(),
          //         tap(() => {
          //             this.paginator.pageIndex = 0;
          //             this.loadZonesPage();
          //         })
          //     )
          //     .subscribe();


          // fromEvent(this.filterOthers?.nativeElement,'change')
          //     .pipe(
          //         debounceTime(150), 
          //         distinctUntilChanged(),
          //         tap(() => {
          //             this.paginator.pageIndex = 0;
          //             this.loadZonesPage();
          //         })
          //     )
          //     .subscribe();

          merge(this.sort.sortChange, this.paginator.page)
              .pipe(
                  tap(() => this.loadZonesPage())
              )
              .subscribe();
      });
  }

  loadZonesPage() {
      console.log('loadZonesPage()');
      console.log(this.paginator.pageIndex)
      this.dataSource?.loadZones(
          0,
          [
              // { "col": "crc", "op" : "like", "val": this.filterCrc?.nativeElement.value},
              // { "col": "name", "op" : "like", "val": this.filterName?.nativeElement.value}
          ],
          this.sort.active,
          this.sort.direction,
          this.paginator.pageIndex + 1,
          this.paginator.pageSize);
  }

  onZone() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.width = "530px";
    dialogConfig.autoFocus = false;
    dialogConfig.data = {};
    const dialogRef = this.dialog.open(ZoneAddUpdateComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(
        data => {
          this.loadZonesPage()
        }
    );
  }

  onEdit(row: any) {
    console.log(row)
    const dialogConfig = new MatDialogConfig();
    dialogConfig.width = "530px";
    dialogConfig.autoFocus = false;
    dialogConfig.data = {
      id: row.id
    };
    const dialogRef = this.dialog.open(ZoneAddUpdateComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(
        data => {
          if(data) this.loadZonesPage()
        }
    );
  }

}
