import {CollectionViewer, DataSource} from "@angular/cdk/collections";
import {Observable, BehaviorSubject, of} from "rxjs";
import {catchError, finalize} from "rxjs/operators";
import { Zone } from "src/app/_models";

import { ZoneService } from '../../../_services';



export class ZonesDatasource implements DataSource<Zone> {

    totalItems = 0;
    
    private ZonesSubject = new BehaviorSubject<Zone[]>([]);

    private loadingSubject = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingSubject.asObservable();

    constructor(private ZoneService: ZoneService) {

    }

    loadZones(courseId:number,
                filters:any[],
                sortBy:string,
                sortDirection:string,
                pageNumber:number,
                pageSize:number) {

        this.loadingSubject.next(true);
        this.ZoneService.findAll(courseId, filters, sortBy, sortDirection,
            pageNumber, pageSize).pipe(
                catchError(() => of([])),
                finalize(() => this.loadingSubject.next(false))
            )
            .subscribe(data => {
                this.totalItems = data.total;
                return this.ZonesSubject.next(data.data);
			});

    }

    connect(collectionViewer: CollectionViewer): Observable<Zone[]> {
		// bind observer
        console.log("Connecting data source");
        return this.ZonesSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.ZonesSubject.complete();
        this.loadingSubject.complete();
    }

}

