import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ZoneListingComponent } from './zone-listing';
import { ZoneComponent } from './zone.component';

const routes: Routes = [
  {
    path: '', component: ZoneComponent, children: [
      { path: 'zone-list', component: ZoneListingComponent },
      { path: '', redirectTo: 'zone-list' }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ZoneRoutingModule { }
