import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ZoneComponent } from './zone.component';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatListModule } from '@angular/material/list';
import { MatTableModule } from '@angular/material/table';
import { ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { SharedModule } from '../shared/shared.module';
import { MatSelectModule } from '@angular/material/select';
import { ZoneAddUpdateComponent } from './zone-add-update/zone-add-update.component';
import { ZoneListingComponent } from './zone-listing/zone-listing.component';
import { ZoneRoutingModule } from './zone-routing.module';



@NgModule({
  declarations: [
    ZoneComponent,
    ZoneAddUpdateComponent,
    ZoneListingComponent
  ],
  imports: [
    CommonModule,
    ZoneRoutingModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatPaginatorModule,
    MatSortModule,
    MatListModule,
    MatTableModule,
    ReactiveFormsModule,
    MatInputModule,
    SharedModule,
    MatSelectModule
  ]
})
export class ZoneModule { }
