import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DailyCallReportComponent } from './daily-call-report.component';

describe('DailyCallReportComponent', () => {
  let component: DailyCallReportComponent;
  let fixture: ComponentFixture<DailyCallReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DailyCallReportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DailyCallReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
