import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DailyCallReportListComponent } from './daily-call-report-list/daily-call-report-list.component';
import { DailyCallReportComponent } from './daily-call-report.component';

const routes: Routes = [{ path: '', component: DailyCallReportComponent, children: [
  { path: 'daily-call-report-list', component: DailyCallReportListComponent },
  { path: '', redirectTo: 'daily-call-report-list' }
] }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DailyCallReportRoutingModule { }
