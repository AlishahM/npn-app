import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DailyCallReportListComponent } from './daily-call-report-list.component';

describe('DailyCallReportListComponent', () => {
  let component: DailyCallReportListComponent;
  let fixture: ComponentFixture<DailyCallReportListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DailyCallReportListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DailyCallReportListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
