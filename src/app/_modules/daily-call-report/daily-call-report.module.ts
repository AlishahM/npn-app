import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DailyCallReportRoutingModule } from './daily-call-report-routing.module';
import { DailyCallReportComponent } from './daily-call-report.component';
import { DailyCallReportListComponent } from './daily-call-report-list/daily-call-report-list.component';


@NgModule({
  declarations: [
    DailyCallReportComponent,
    DailyCallReportListComponent
  ],
  imports: [
    CommonModule,
    DailyCallReportRoutingModule
  ]
})
export class DailyCallReportModule { }
