import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MemorandumAddUpdateComponent } from './memorandum-add-update/memorandum-add-update.component';
import { MemorandumListComponent } from './memorandum-list/memorandum-list.component';
import { MemorandumComponent } from './memorandum.component';

const routes: Routes = [{
  path: '', component: MemorandumComponent, children: [
    { path: 'memorandum-list', component: MemorandumListComponent },
    { path: 'add-memorandum', component: MemorandumAddUpdateComponent },
    { path: '', redirectTo: 'memorandum-list' }
  ]
}];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MemorandumRoutingModule { }
