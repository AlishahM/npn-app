import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MemorandumListComponent } from './memorandum-list.component';

describe('MemorandumListComponent', () => {
  let component: MemorandumListComponent;
  let fixture: ComponentFixture<MemorandumListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MemorandumListComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MemorandumListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
