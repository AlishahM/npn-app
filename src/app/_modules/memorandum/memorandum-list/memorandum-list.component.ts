import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { merge } from 'rxjs';
import { tap } from 'rxjs/operators';
import { MemorandumService } from 'src/app/_services';
import { MemorandumAddUpdateComponent } from '../memorandum-add-update/memorandum-add-update.component';
import { MemorandumDatasource } from './memorandum-datasource';

@Component({
  selector: 'app-memorandum-list',
  templateUrl: './memorandum-list.component.html',
  styleUrls: ['./memorandum-list.component.scss']
})
export class MemorandumListComponent implements OnInit {

  dataSource: MemorandumDatasource | any;

  displayedColumns = ["id", "date", "subject", "message"];

  @ViewChild(MatPaginator) paginator: MatPaginator | any;
  @ViewChild(MatSort)
  sort: MatSort = new MatSort;

  @ViewChild('reload') reload: ElementRef | undefined;

  constructor(
    private memorandumService: MemorandumService,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.dataSource = new MemorandumDatasource(this.memorandumService);
  }

  ngAfterViewInit() {
    Promise.resolve().then(() => {

      this.dataSource?.loadMemorandum(0, [], '', 'asc', 1, 10);

      this.sort?.sortChange.subscribe(() => this.paginator.pageIndex = 0);

      merge(this.sort.sortChange, this.paginator.page)
        .pipe(
          tap(() => this.loadMemorandumPage())
        )
        .subscribe();
    });
  }

  loadMemorandumPage() {
    console.log('loadMemorandumPage()');
    console.log(this.paginator.pageIndex)
    this.dataSource?.loadMemorandum(
      0,
      [
        // { "col": "crc", "op" : "like", "val": this.filterCrc?.nativeElement.value},
        // { "col": "name", "op" : "like", "val": this.filterName?.nativeElement.value}
      ],
      this.sort.active,
      this.sort.direction,
      this.paginator.pageIndex + 1,
      this.paginator.pageSize);
  }

  onMemorandum() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.width = "930px";
    dialogConfig.autoFocus = false;
    dialogConfig.data = {};
    const dialogRef = this.dialog.open(MemorandumAddUpdateComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(
      data => {
        this.loadMemorandumPage()
      }
    );
  }

}
