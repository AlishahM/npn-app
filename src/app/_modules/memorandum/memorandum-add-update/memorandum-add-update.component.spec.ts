import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MemorandomAddUpdateComponent } from './memorandum-add-update.component';

describe('MemorandomAddUpdateComponent', () => {
  let component: MemorandomAddUpdateComponent;
  let fixture: ComponentFixture<MemorandomAddUpdateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MemorandomAddUpdateComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MemorandomAddUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
