import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { validateAllFormFields } from 'src/app/_helpers';
import { MemorandumService } from 'src/app/_services';

@Component({
  selector: 'app-memorandum-add-update',
  templateUrl: './memorandum-add-update.component.html',
  styleUrls: ['./memorandum-add-update.component.scss']
})
export class MemorandumAddUpdateComponent implements OnInit {

  loading: boolean = false;
  updated: boolean = false;
  errors: any;

  // Initializing Form with fields
  memorandumForm = new FormGroup({
    id: new FormControl(null, []),
    date: new FormControl('', [Validators.required]),
    subject: new FormControl('', [Validators.required]),
    message: new FormControl('', [Validators.required]),
  });

  constructor(
    public dialogRef: MatDialogRef<MemorandumAddUpdateComponent>,
    private memorandumService: MemorandumService,
  ) { }

  ngOnInit(): void {
  }

  onSubmit(): void {
    if (this.memorandumForm.valid) {
      this.memorandumService.store(this.memorandumForm.value).subscribe(
        data => {
          console.log(data)
          this.onClose()
        },
        err => {
          console.log(err)
          this.errors = err.errors
        }
      )
    } else
      validateAllFormFields(this.memorandumForm);
  }

  onClear(): void {
    this.memorandumForm.reset()
  }

  onClose() {
    this.dialogRef.close({ updated: this.updated })
  }

}
