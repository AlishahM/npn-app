import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MemorandumRoutingModule } from './memorandum-routing.module';
import { MemorandumComponent } from './memorandum.component';
import { MemorandumAddUpdateComponent } from './memorandum-add-update/memorandum-add-update.component';
import { MemorandumListComponent } from './memorandum-list/memorandum-list.component';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatListModule } from '@angular/material/list';
import { MatTableModule } from '@angular/material/table';
import { ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { SharedModule } from '../shared/shared.module';
import { MatSelectModule } from '@angular/material/select';


@NgModule({
  declarations: [
    MemorandumComponent,
    MemorandumAddUpdateComponent,
    MemorandumListComponent
  ],
  imports: [
    CommonModule,
    MemorandumRoutingModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatPaginatorModule,
    MatSortModule,
    MatListModule,
    MatTableModule,
    ReactiveFormsModule,
    MatInputModule,
    SharedModule,
    MatSelectModule
  ]
})
export class MemorandumModule { }
