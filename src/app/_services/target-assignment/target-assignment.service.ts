import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TargetAssignmentService {

  constructor(private http: HttpClient) { }

  find(id: any): Observable<any> {
    return this.http.post(`${environment.apiUrl}/targetAssignment/get/` + id, {
      id: id
    }).pipe(map((data: any) => { return data }));
  }

  findAll(
    Id: number,
    filters: any = [],
    sortBy = "id",
    sortOrder = "asc",
    pageNumber = 1,
    pageSize = 10
  ): Observable<any> {

    return this.http.post(`${environment.apiUrl}/targetAssignment`,
      { 'sortOrder': sortOrder, 'sortBy': sortBy, 'page': pageNumber.toString(), 'pageSize': pageSize.toString(), 'filters': filters }
    ).pipe(map((data: any) => { return data }));
  }

  getTargetAssignmentDetail(
    companyId: number,
    zoneId: number,
    year: number,
    month: number
  ): Observable<any> {

    return this.http.post(`${environment.apiUrl}/targetAssignment/targetAssignmentDetail`,
      { 'companyId': companyId, 'zoneId': zoneId, 'year': year, 'month': month }
    ).pipe(map((data: any) => { return data }));
  }

  store(targetAssignment: any): Observable<any> {
    targetAssignment = {...targetAssignment, total_month_expense: 0, total_annual_expense: 0, total_claim_expense: 0, total_promotional_expense: 0}
    return this.http
      .post(`${environment.apiUrl}/targetAssignment/create`, targetAssignment)
      .pipe(catchError(this.handleError));
  }

  update(targetAssignment: any): Observable<any> {
    return this.http
      .post(`${environment.apiUrl}/targetAssignment/update`, targetAssignment)
      .pipe(catchError(this.handleError));
  }

  delete(id: string): Observable<any> {
    return this.http.post(`${environment.apiUrl}/targetAssignment/delete`, {
      id: id
    });
  }

  handleError(error: HttpErrorResponse) {
    console.log(error);
    return throwError(error);
  }
}
