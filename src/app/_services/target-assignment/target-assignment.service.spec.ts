import { TestBed } from '@angular/core/testing';

import { TargetAssignmentService } from './target-assignment.service';

describe('TargetAssignmentService', () => {
  let service: TargetAssignmentService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TargetAssignmentService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
