import { TestBed } from '@angular/core/testing';

import { SalesExpenseService } from './sales-expense.service';

describe('SalesExpenseService', () => {
  let service: SalesExpenseService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SalesExpenseService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
