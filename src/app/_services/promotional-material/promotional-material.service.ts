import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PromotionalMaterialService {

  constructor(private http: HttpClient) { }

  find(id: any): Observable<any> {
    return this.http.post(`${environment.apiUrl}/promotionalMaterial/get/` + id, {
      id: id
    }).pipe(map((data: any) => { return data }));
  }

  findAll(
    Id: number,
    filters: any = [],
    sortBy = "id",
    sortOrder = "asc",
    pageNumber = 1,
    pageSize = 10
  ): Observable<any> {

    return this.http.post(`${environment.apiUrl}/promotionalMaterial`,
      { 'sortOrder': sortOrder, 'sortBy': sortBy, 'page': pageNumber.toString(), 'pageSize': pageSize.toString(), 'filters': filters }
    ).pipe(map((data: any) => { return data }));
  }

  store(promotionalMaterial: any): Observable<any> {
    return this.http
      .post(`${environment.apiUrl}/promotionalMaterial/create`, promotionalMaterial)
      .pipe(catchError(this.handleError));
  }

  update(promotionalMaterial: any): Observable<any> {
    return this.http
      .post(`${environment.apiUrl}/promotionalMaterial/update`, promotionalMaterial)
      .pipe(catchError(this.handleError));
  }

  delete(id: string): Observable<any> {
    return this.http.post(`${environment.apiUrl}/promotionalMaterial/delete`, {
      id: id
    });
  }

  handleError(error: HttpErrorResponse) {
    console.log(error);
    return throwError(error);
  }
}
