import { TestBed } from '@angular/core/testing';

import { PromotionalMaterialService } from './promotional-material.service';

describe('PromotionalMaterialService', () => {
  let service: PromotionalMaterialService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PromotionalMaterialService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
