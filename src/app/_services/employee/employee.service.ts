import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor(private http: HttpClient) { }

  find(id: any): Observable<any> {
    return this.http.post(`${environment.apiUrl}/employee/get/` + id, {
      id: id
    }).pipe(map((data: any) => { return data }));
  }

  findAll(
    Id: number,
    filters: any = [],
    sortBy = "id",
    sortOrder = "asc",
    pageNumber = 1,
    pageSize = 10
  ): Observable<any> {

    return this.http.post(`${environment.apiUrl}/employee`,
      { 'sortOrder': sortOrder, 'sortBy': sortBy, 'page': pageNumber.toString(), 'pageSize': pageSize.toString(), 'filters': filters }
    ).pipe(map((data: any) => { return data }));
  }

  store(employee: any): Observable<any> {
    return this.http
      .post(`${environment.apiUrl}/register`, employee)
      .pipe(catchError(this.handleError));
  }

  update(employee: any): Observable<any> {
    return this.http
      .post(`${environment.apiUrl}/employee/update`, employee)
      .pipe(catchError(this.handleError));
  }

  delete(id: string): Observable<any> {
    return this.http.post(`${environment.apiUrl}/employee/delete`, {
      id: id
    });
  }

  handleError(error: HttpErrorResponse) {
    console.log(error);
    return throwError(error);
  }
}
