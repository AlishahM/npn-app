import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { Employees } from 'src/app/_models/employees';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  private currentEmployeeSubject: BehaviorSubject<Employees>;
  public currentEmployee: Observable<Employees>;

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(private _http: HttpClient, private router: Router) {
    this.currentEmployeeSubject = new BehaviorSubject<Employees>(JSON.parse(localStorage.getItem('currentEmployee') || '{}'));
    this.currentEmployee = this.currentEmployeeSubject.asObservable();
  }

  public get currentEmployeeValue(): any {
    return this.currentEmployeeSubject.value;
  }

  login(loginData: any): Observable<any> {
    return this._http.post(`${environment.apiUrl}/login`, loginData, this.httpOptions)
      .pipe(
        tap((employee: any) => {
          localStorage.setItem('currentEmployee', JSON.stringify(employee));
          this.currentEmployeeSubject.next(employee);
          return employee;
        }),
        catchError(this._handleError)
      );
  }

  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('currentEmployee');
    this.currentEmployeeSubject.next(<any>{});
    this.router.navigate(['login']);
  }

  private _handleError(error: HttpErrorResponse) {
    return throwError(error);
  }
}
