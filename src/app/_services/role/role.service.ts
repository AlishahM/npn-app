import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RoleService {

  constructor(private http: HttpClient) { }

  find(id: any): Observable<any> {
    return this.http.post(`${environment.apiUrl}/role/get/` + id, {
      id: id
    }).pipe(map((data: any) => { return data }));
  }

  findAll(
    Id: number,
    filters: any = [],
    sortBy = "id",
    sortOrder = "asc",
    pageNumber = 1,
    pageSize = 10
  ): Observable<any> {

    // return of(MOCK_DATA);

    return this.http.post(`${environment.apiUrl}/role`,
      { 'sortOrder': sortOrder, 'sortBy': sortBy, 'page': pageNumber.toString(), 'pageSize': pageSize.toString(), 'filters': filters }
    ).pipe(map((data: any) => { return data }));
  }

  store(role: Zone): Observable<any> {
    return this.http
      .post(`${environment.apiUrl}/role/create`, role)
      .pipe(catchError(this.handleError));
  }

  update(role: Zone): Observable<any> {
    console.log(role)
    return this.http
      .post(`${environment.apiUrl}/role/update`, role)
      .pipe(catchError(this.handleError));
  }

  delete(id: string): Observable<any> {
    return this.http.post(`${environment.apiUrl}/role/delete`, {
      id: id
    });
  }

  handleError(error: HttpErrorResponse) {
    console.log(error);
    return throwError(error);
  }
}
