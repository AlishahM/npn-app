import { TestBed } from '@angular/core/testing';

import { PurchaseOrderDetailService } from './purchase-order-detail.service';

describe('PurchaseOrderDetailService', () => {
  let service: PurchaseOrderDetailService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PurchaseOrderDetailService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
