import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DistributorLedgerService {

  constructor(private http: HttpClient) { }

  findAll(
    Id: number,
    filters: any = [],
    sortBy = "id",
    sortOrder = "asc",
    pageNumber = 1,
    pageSize = 10
  ): Observable<any> {

    return this.http.post(`${environment.apiUrl}/distributorLedger`,
      { 'sortOrder': sortOrder, 'sortBy': sortBy, 'page': pageNumber.toString(), 'pageSize': pageSize.toString(), 'filters': filters }
    ).pipe(map((data: any) => { return data }));
  }
  
  store(distributorLedger: any): Observable<any> {
    return this.http
      .post(`${environment.apiUrl}/distributorLedger/create`, distributorLedger)
      .pipe(catchError(this.handleError));
  }

  handleError(error: HttpErrorResponse) {
    return throwError(error);
  }
}
