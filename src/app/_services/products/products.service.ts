import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Products } from 'src/app/_models';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  constructor(private http: HttpClient) { }

  find(id: any): Observable<any> {
    return this.http.post(`${environment.apiUrl}/product/get/` + id, {
      id: id
    }).pipe(map((data: any) => { return data }));
  }

  findAll(
    Id: number,
    filters: any = [],
    sortBy = "id",
    sortOrder = "asc",
    pageNumber = 1,
    pageSize = 10
  ): Observable<any> {

    // return of(MOCK_DATA);

    return this.http.post(`${environment.apiUrl}/product`,
      { 'sortOrder': sortOrder, 'sortBy': sortBy, 'page': pageNumber.toString(), 'pageSize': pageSize.toString(), 'filters': filters }
    ).pipe(map((data: any) => { return data }));
  }

  store(product: Products): Observable<any> {
    // product.id = '00000000-0000-0000-0000-000000000000';
    // '00000000-0000-0000-0000-000000000000'
    return this.http
      .post(`${environment.apiUrl}/product/create`, product)
      .pipe(catchError(this.handleError));
  }

  update(product: Products): Observable<any> {
    return this.http
      .post(`${environment.apiUrl}/product/update`, product)
      .pipe(catchError(this.handleError));
  }

  delete(id: string): Observable<any> {
    return this.http.post(`${environment.apiUrl}/product/delete`, {
      id: id
    });
  }

  handleError(error: HttpErrorResponse) {
    console.log(error);
    return throwError(error);
  }
}
