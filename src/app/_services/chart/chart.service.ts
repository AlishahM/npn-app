import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ChartService {

  constructor(private http: HttpClient) { }

  getProductAnalysisByTarget(
    companyId: number,
    zoneId: number,
    year: number,
    employeeId: number
  ): Observable<any> {

    return this.http.post(`${environment.apiUrl}/productAnalysisByTargetChart`,
      { 'companyId': companyId, 'zoneId': zoneId, 'year': year, 'employeeId': employeeId }
    ).pipe(map((data: any) => { return data }));
  }

  getProductAnalysisByProduct(
    companyId: number,
    year: number,
    productId: number
  ): Observable<any> {

    return this.http.post(`${environment.apiUrl}/productAnalysisByProductChart`,
      { 'companyId': companyId, 'year': year, 'productId': productId }
    ).pipe(map((data: any) => { return data }));
  }

  handleError(error: HttpErrorResponse) {
    console.log(error);
    return throwError(error);
  }
}
