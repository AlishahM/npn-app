import { TestBed } from '@angular/core/testing';

import { SalesExpenseProductService } from './sales-expense-product.service';

describe('SalesExpenseProductService', () => {
  let service: SalesExpenseProductService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SalesExpenseProductService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
