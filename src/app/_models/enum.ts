export enum UserTypes {
  ADMIN = 1,
  EMPLOYEE = 2,
  DISTRIBUTOR = 3,
  MANAGER = 4,
}