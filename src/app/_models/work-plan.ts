import { WorkPlanDetails } from "./work-plan-details";

export class WorkPlan {
    id: number | null | undefined;
    from_date: Date | string | undefined;
    to_date: Date | string | undefined;
    month: string | undefined;
    year: string | undefined;
    work_plan_details: WorkPlanDetails[] | undefined;

    deserialize(input: any): this {
        Object.assign(this, input);
        return this;
    }

    compose(): this {
        return this;
    }
}