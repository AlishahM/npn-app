export class WorkPlanDetails {
    id: number | undefined;
    date: Date | string | undefined;
    revised_date: Date | string | undefined;
    work_type: number | string | undefined;
    doctor: number | string | undefined;
    morning_area: string | undefined;
    morning_time: string | undefined;
    evening_area: string | undefined;
    evening_time: string | undefined;

    deserialize(input: any): this {
        Object.assign(this, input);
        return this;
    }

    compose(): this {
        return this;
    }
}