import { TitleCasePipe } from "@angular/common";

export class Company {
    id: number | null | undefined;
    name: string | null | undefined;
    zone_id: any | null | undefined;

    deserialize(input: any): this {
        Object.assign(this, input);
        return this;
    }

    compose(): this {
        this.name = new TitleCasePipe().transform(this.name);
        return this;
    }
}