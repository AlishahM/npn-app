export class Memorandum {
    id: number | null | undefined;
    date: string | null | undefined;
    subject: string | null | undefined;
    message: string | null | undefined;

    deserialize(input: any): this {
        Object.assign(this, input);
        return this;
    }

    compose(): this {
        return this;
    }
}