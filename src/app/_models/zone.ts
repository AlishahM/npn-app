import { TitleCasePipe, UpperCasePipe } from "@angular/common";

export class Zone {
    id: number | null | undefined;
    zone: string | null | undefined;
    area_name: string | null | undefined;

    deserialize(input: any): this {
        Object.assign(this, input);
        return this;
    }

    compose(): this {
        this.zone = new UpperCasePipe().transform(this.zone);
        this.area_name = new TitleCasePipe().transform(this.area_name);
        return this;
    }
}