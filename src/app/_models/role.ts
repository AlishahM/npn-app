export class Role {
    id: number | null | undefined;
    role: number | null | undefined;

    deserialize(input: any): this {
        Object.assign(this, input);
        return this;
    }

    compose(): this {
        return this;
    }
}