export class PromotionalMaterial {
    id: number | null | undefined;
    name: string | null | undefined;
    price: number | null | undefined;
    quantity: number | null | undefined;

    deserialize(input: any): this {
        Object.assign(this, input);
        return this;
    }

    compose(): this {
        return this;
    }
}