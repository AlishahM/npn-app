export class Products {
    id: number | undefined;
    name: string | undefined;
    price: string | undefined;
    deleted_at: string | undefined;
}
