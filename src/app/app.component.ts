import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from './_services';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'npn-app';

  employee: any

  constructor(
    private authenticationService: AuthenticationService, private router: Router
  ) {
    this.employee = this.authenticationService.currentEmployeeValue;
    if (Object.entries(this.employee).length === 0)
      this.router.navigate(['login']);
  }
}
