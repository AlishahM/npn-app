import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', loadChildren: () => import('./_modules/shared/shared.module').then(m => m.SharedModule) },
  { path: 'products', loadChildren: () => import('./_modules/products/products.module').then(m => m.ProductsModule) },
  { path: 'company', loadChildren: () => import('./_modules/company/company.module').then(m => m.CompanyModule) },
  { path: 'zone', loadChildren: () => import('./_modules/zone/zone.module').then(m => m.ZoneModule) },
  { path: 'employees', loadChildren: () => import('./_modules/employees/employees.module').then(m => m.EmployeesModule) },
  { path: 'promotional-material', loadChildren: () => import('./_modules/promotional-material/promotional-material.module').then(m => m.PromotionalMaterialModule) },
  { path: 'memorandum', loadChildren: () => import('./_modules/memorandum/memorandum.module').then(m => m.MemorandumModule) },
  { path: 'daily-call-report', loadChildren: () => import('./_modules/daily-call-report/daily-call-report.module').then(m => m.DailyCallReportModule) },
  { path: 'monthly-work-plan', loadChildren: () => import('./_modules/monthly-work-plan/monthly-work-plan.module').then(m => m.MonthlyWorkPlanModule) },
  { path: 'reports', loadChildren: () => import('./_modules/reports/reports.module').then(m => m.ReportsModule) },
  { path: 'doctors', loadChildren: () => import('./_modules/doctors/doctors.module').then(m => m.DoctorsModule) },
  { path: 'sales-expense', loadChildren: () => import('./_modules/sales-expense/sales-expense.module').then(m => m.SalesExpenseModule) },
  { path: 'distributor-ledger', loadChildren: () => import('./_modules/distributor-ledger/distributor-ledger.module').then(m => m.DistributorLedgerModule) },
  { path: 'product-analysis', loadChildren: () => import('./_modules/product-analysis/product-analysis.module').then(m => m.ProductAnalysisModule) },
  { path: 'purchase-order', loadChildren: () => import('./_modules/purchase-order/purchase-order.module').then(m => m.PurchaseOrderModule) },
  { path: 'growth-report', loadChildren: () => import('./_modules/growth-report/growth-report.module').then(m => m.GrowthReportModule) },
  { path: 'stock-analysis', loadChildren: () => import('./_modules/stock-analysis/stock-analysis.module').then(m => m.StockAnalysisModule) },
  { path: 'budget-distribution', loadChildren: () => import('./_modules/budget-distribution/budget-distribution.module').then(m => m.BudgetDistributionModule) },
  { path: 'target-assignment', loadChildren: () => import('./_modules/target-assignment/target-assignment.module').then(m => m.TargetAssignmentModule) },
  { path: 'target-sale-product', loadChildren: () => import('./_modules/target-sale-product/target-sale-product.module').then(m => m.TargetSaleProductModule) },
  { path: 'sale-graph', loadChildren: () => import('./_modules/sale-graph/sale-graph.module').then(m => m.SaleGraphModule) },
  { path: 'attendance', loadChildren: () => import('./_modules/attendance/attendance.module').then(m => m.AttendanceModule) },
  { path: 'client-investment-ledger', loadChildren: () => import('./_modules/client-investment-ledger/client-investment-ledger.module').then(m => m.ClientInvestmentLedgerModule) },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
