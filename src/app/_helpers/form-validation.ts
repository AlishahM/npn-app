import { FormControl, FormGroup } from "@angular/forms";

// Check if all the fields satisfy the Validator requirements
export function validateAllFormFields(formData: FormGroup) {
  Object.keys(formData.controls).forEach(field => {

    const control = formData.get(field);

    if (control instanceof FormControl)
      control.markAsTouched({ onlySelf: true });
    else if (control instanceof FormGroup)
      validateAllFormFields(control);
    
  });
}