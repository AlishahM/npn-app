import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';

import { AuthenticationService } from '../_services';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private authenticationService: AuthenticationService) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // add authorization header with jwt token if available
    let currentEmployee = this.authenticationService.currentEmployeeValue;
    // if (currentUser && currentUser.token) {

    request = request.clone({
      setHeaders: { Authorization: `Bearer ${currentEmployee.token}` },
      body: {
        ...request.body
      }
    });
    // }

    return next.handle(request);
  }
}