import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AuthenticationService } from '../_services';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
	constructor(
		private authenticationService: AuthenticationService,
	) { }

	intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
		return next.handle(request).pipe(catchError(err => {
			if (err.status === 401) {
				// auto logout if 401 response returned from api 
				this.authenticationService.logout();
				location.reload();
			} else if (err.status === 400) {
				if (err.error.errorMessage) {
					err.error.errors = err.error.errorMessage
					delete err.error['errorMessage']
				}

				for (var key in err.error.errors) {
					if (!Array.isArray(err.error.errors[key])) {
						err.error.errors[key] = [err.error.errors[key]]
					}
				}

			} else if (err.status === 500) {
				alert("Oops! something went wrong please try again later.");
			}

			return throwError(err);
		}))
	}
}